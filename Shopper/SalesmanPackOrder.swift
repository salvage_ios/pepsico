//
//  SalesmanPackOrder.swift
//  Shopper
//
//  Created by VividMacmini7 on 04/04/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class SalesmanPackOrder: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var orderListTable:UITableView!
    
    
    var sectionTitle = [String]()
    var orderDetailList = [OrderDetailModel]()
    var orderDetailRef = FIRDatabase.database().referenceWithPath("order_Details")
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        getPendingOrderList()
    }
    
    func getPendingOrderList() {
        orderDetailList = [OrderDetailModel]()
        sectionTitle = [String]()
        
        orderDetailRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let detail = OrderDetailModel(snapshot: child as! FIRDataSnapshot)
                    if detail.orderStatus == .OrderTaken {
                        self.orderDetailList.append(detail)
                        self.sectionTitle.append(detail.orderTakenBy)
                    }
                    
                }

                self.sectionTitle = Array(Set(self.sectionTitle))
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.title = "Order To Be Packed (\(self.orderDetailList.count))"
                    self.orderListTable.reloadData()
                })
                
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: UITableView DataSouce:
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sectionTitle.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sectionTitle[section]
        var i = 0
        for content in orderDetailList {
            if section == content.orderTakenBy {
                i += 1
            }
        }
        return i
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PackOrderListCell", forIndexPath: indexPath)
        
        let section = sectionTitle[indexPath.section]
        var item = [OrderDetailModel]()
        for content in orderDetailList {
            if section == content.orderTakenBy {
                item.append(content)
            }
        }
        cell.textLabel?.text = item[indexPath.row].shopName
        cell.detailTextLabel?.text = item[indexPath.row].orderPrice
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,40))
        headerView.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        let areaName = UILabel(frame:  CGRectMake(15,0,self.view.frame.size.width/2,40))
        areaName.text = sectionTitle[section]
        areaName.textColor = UIColor.blackColor()
        areaName.font = UIFont.AppFontBold()
        headerView.addSubview(areaName)
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let section = sectionTitle[indexPath.section]
        var item = [OrderDetailModel]()
        for content in orderDetailList {
            if section == content.orderTakenBy {
                item.append(content)
            }
        }
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PackOrder") as! PackOrder
        vc.orderDetail = item[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}
