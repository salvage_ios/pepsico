//
//  SalesmanTakeOrder.swift
//  Shopper
//
//  Created by VividMacmini7 on 30/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class SalesmanTakeOrder: UIViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITextFieldDelegate {

    //MARK: IBOutlets
    @IBOutlet var shopListTable:UITableView!
    @IBOutlet var previewTable:UITableView!
    @IBOutlet var previwContainer:UIView!
    @IBOutlet var orderPrice:UILabel!
    @IBOutlet var actualPrice:UILabel!
    @IBOutlet var tax:UILabel!
    @IBOutlet var discount:UILabel!
    
    
    //MARK: Models
    var shopDetails = [CustomerModel]()
    var stockDetails = [StockItem]()
    var stockNames = [String]()
    
    var orderListModel = [OrderModel]()
    var previewOrderList = [OrderModel]()
    
    var orderDetailsModel = [OrderDetailModel]()
    var previewOrderDetailModel:OrderDetailModel!
    
    var previewCustomerModel:CustomerModel!
    
    //MARK: FIRReference:
    let stockRef = FIRDatabase.database().referenceWithPath("stock_items")
    let orderListRef = FIRDatabase.database().referenceWithPath("order_list")
    let orderDetailRef = FIRDatabase.database().referenceWithPath("order_Details")
    
    //MARK: Variables:
    var salesman:SalesmanModel!
    var snapshot:FIRDataSnapshot!
    var expandCell:NSMutableArray!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Setup PreviewView:
        var shadowLayer: CAShapeLayer!
        shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: previwContainer.bounds, cornerRadius: 5).CGPath
        shadowLayer.fillColor = UIColor.whiteColor().CGColor
        shadowLayer.shadowColor = UIColor.lightGrayColor().CGColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 1.0, height:1.0)
        shadowLayer.shadowOpacity = 0.8
        shadowLayer.shadowRadius = 1
        
        previwContainer.layer.insertSublayer(shadowLayer, atIndex: 0)
        previwContainer.backgroundColor = UIColor.redColor()
        previewTable.estimatedRowHeight = 44
        previewTable.tableFooterView = UIView()
        
        //Get shopDetails in this area:
        expandCell = NSMutableArray()
        for _ in shopDetails {
            expandCell.addObject(false)
            
            
            let detail = OrderDetailModel(id: "", qty: "0", price: "0", takenBy: salesman.salesmanName, date: "", shopName: "", shopPhone: "", shopOwner: "", shopArea: "", orderStatus: OrderStatus.NoOrder, latLng: "")
            orderDetailsModel.append(detail)
        }
        
        //Get Stock Details:
        stockRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            if !(snapshot.value is NSNull) {
                self.snapshot = snapshot
                self.loadStockList()
            }
        })
    }

    func loadStockList() {
        self.orderListModel = [OrderModel]()
        for child in snapshot.children {
            let stock = StockItem(snapshot: child as! FIRDataSnapshot)
            self.stockDetails.append(stock)
            self.stockNames.append(stock.name)
            
            let order = OrderModel(productName: stock.name, productPrice: stock.price, productQty: "0", productTotlaPrice: "0", productTax: stock.tax, productDisc: stock.discount)
            self.orderListModel.append(order)
        }

        self.shopListTable.reloadData()
        self.title = "\(self.shopDetails[0].shopArea)"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: UITableviewDataSource:
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return  tableView != previewTable ? shopDetails.count : 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView != previewTable {
            let isExpanded = expandCell.objectAtIndex(section) as! Bool
            return isExpanded ? stockDetails.count + 1 : 1
        }
        return previewOrderList.count + 1
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView != previewTable {
            if indexPath.row ==  0{
                let cell = tableView.dequeueReusableCellWithIdentifier("TakeOrderCellOne", forIndexPath: indexPath) as! TakeOrderCellOne
                cell.shopName.text = shopDetails[indexPath.section].shopName
                
                cell.cancelWidthConstraint.constant = expandCell[indexPath.section] as! Bool ? 85 : 0
                cell.noOrder.tag = expandCell[indexPath.section] as! Bool ? 20 : 10
                cell.previewOrder.hidden = !(expandCell[indexPath.section] as! Bool)
                cell.takeOrder.hidden = expandCell[indexPath.section] as! Bool
                
                cell.previewOrder.addTarget(self, action: #selector(previewOrder(_:)), forControlEvents: .TouchUpInside)
                cell.previewOrder.tag = indexPath.section
                
                cell.takeOrder.addTarget(self, action: #selector(takeOrder(_:)), forControlEvents: .TouchUpInside)
                cell.takeOrder.tag = indexPath.section
                
                cell.noOrder.addTarget(self, action: #selector(noOrder(_:)), forControlEvents: .TouchUpInside)
                cell.noOrder.tag = indexPath.section
                
                let status = orderDetailsModel[indexPath.section].orderStatus
                if status == .OrderTaken {
                    cell.takeOrder.hidden = true
                    cell.previewOrder.hidden = false
                    cell.previewOrder.backgroundColor = UIColor.appThemeYellow()
                    cell.previewOrder.setTitle("Order Taken", forState: .Normal)
                    cell.noOrder.setTitle("Edit", forState: .Normal)
                }else if status == .OrderPacked {
                    cell.previewOrder.backgroundColor = UIColor.orangeColor()
                    cell.previewOrder.setTitle("Order Packed", forState: .Normal)
                    cell.noOrder.setTitle("Hide", forState: .Normal)
                }else if status == .OrderDelivered {
                    cell.previewOrder.backgroundColor = UIColor.redColor()
                    cell.previewOrder.setTitle("Order Delivered", forState: .Normal)
                    cell.noOrder.setTitle("Hide", forState: .Normal)
                }
                return cell
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier("TakeOrderCellTwo", forIndexPath: indexPath) as! TakeOrderCellTwo
            cell.productName.text = stockDetails[indexPath.row - 1].name
            
            let myAttribute = [NSFontAttributeName: UIFont(name: "TamilSangamMN", size: 15)!]
            
            //Price
            let str = "Price: \(stockDetails[indexPath.row - 1].price) ₹"
            var attributeStr = NSMutableAttributedString(string: str, attributes: myAttribute )
            attributeStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: NSRange(location: 0, length: 5))
            attributeStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.blackColor() , range: NSRange(location: 6, length: "\(stockDetails[indexPath.row - 1].price) ₹".characters.count))
            cell.stockPrice.attributedText = attributeStr
            
            //Qty
            attributeStr = NSMutableAttributedString(string: "Qty: \(stockDetails[indexPath.row - 1].Qty)", attributes: myAttribute )
            attributeStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: NSRange(location: 0, length: 3))
            attributeStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.blackColor() , range: NSRange(location: 4, length: stockDetails[indexPath.row - 1].Qty.characters.count + 1))
            cell.stockQty.attributedText = attributeStr
            
            cell.orderStepper.addTarget(self, action: #selector(stepperAction(_:)), forControlEvents: .ValueChanged)
            cell.orderStepper.minimumValue = 0
            cell.orderStepper.value = Double(orderListModel[indexPath.row - 1].productQty)!
            cell.orderStepper.tag = indexPath.row
            cell.orderQty.tag = indexPath.row
            cell.orderQty.delegate = self
            
            let orderDetail = orderListModel[indexPath.row - 1]
            cell.orderQty.text = orderDetail.productQty
            cell.indicatorImage.tintColor = UIColor.appThemeGreen()
            cell.indicatorImage.image = orderDetail.productQty != "0" ? UIImage(named: "Okay") : UIImage(named: "NotOkay")
            cell.backgroundColor = orderDetail.productQty != "0" ? UIColor.selectedStockBG() : UIColor.whiteColor()
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("PreviewTableCell", forIndexPath: indexPath) as! PreviewTableCell
        if indexPath.row == 0 {
            cell.sNo.text = "#"
            cell.sNo.font = UIFont.AppFontBold()
            
            cell.productName.text = "Product Name"
            cell.productName.font = UIFont.AppFontBold()
            
            cell.productQty.text = "Qty"
            cell.productQty.font = UIFont.AppFontBold()
            
            cell.productPrice.text = "Price"
            cell.productPrice.font = UIFont.AppFontBold()
            
            cell.productTotalPrice.text = "Total"
            cell.productTotalPrice.font = UIFont.AppFontBold()
            
            cell.productTax.text = "Tax"
            cell.productTax.font = UIFont.AppFontBold()
            
            cell.productDisc.text = "Disc"
            cell.productDisc.font = UIFont.AppFontBold()
            
        }else{
            
            cell.backgroundColor = even(indexPath.row) ? UIColor.selectedStockBG() : UIColor.whiteColor()
            
            cell.sNo.text = String(indexPath.row)
            cell.sNo.font = UIFont.AppFontRegular()
            
            cell.productName.text = previewOrderList[indexPath.row - 1].productName
            cell.productName.font = UIFont.AppFontRegular()
            
            cell.productQty.text = previewOrderList[indexPath.row - 1].productQty
            cell.productQty.font = UIFont.AppFontRegular()
            
            cell.productPrice.text = previewOrderList[indexPath.row - 1].productPrice
            cell.productPrice.font = UIFont.AppFontRegular()
            
            cell.productTotalPrice.text = previewOrderList[indexPath.row - 1].productTotalPrice
            cell.productTotalPrice.font = UIFont.AppFontRegular()
            
            cell.productTax.text = "\(previewOrderList[indexPath.row - 1].productTax)%"
            cell.productTax.font = UIFont.AppFontRegular()
            
            cell.productDisc.text = "\(previewOrderList[indexPath.row - 1].productDisc)%"
            cell.productDisc.font = UIFont.AppFontRegular()
            
        }
        return cell
    }
    
    //MARK:UITableViwDelegate:
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.view.endEditing(true)
        if indexPath.row != 0 {
            if orderListModel[indexPath.row - 1].productQty == "1" {
                orderListModel[indexPath.row - 1].productQty = "0"
            }else if orderListModel[indexPath.row - 1].productQty == "0" {
                orderListModel[indexPath.row - 1].productQty = "1"
            }
            
            
            shopListTable.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView != previewTable {
            return indexPath.row == 0 ? 70 : 90
        }
        return UITableViewAutomaticDimension
    }
    
    

    //MARK: IBAction:
    @IBAction func stepperAction(sender:UIStepper) {

        self.view.endEditing(true)
        let section = expandCell.indexOfObject(true)
        let indexPath = NSIndexPath(forRow: sender.tag, inSection:section)
        let stepperOldValue = Double(orderListModel[indexPath.row - 1].productQty)
        let cell = self.shopListTable.cellForRowAtIndexPath(indexPath) as! TakeOrderCellTwo
        var oldValue:Int = Int(cell.orderQty.text!)!
        if sender.value > stepperOldValue {
            oldValue += 1
        }else{
            oldValue -= 1
        }
        
        cell.orderQty.text = oldValue < 0 ? "0" : String(oldValue)
        orderListModel[indexPath.row - 1].productQty = oldValue < 0 ? "0" : String(oldValue)
        shopListTable.reloadData()
    }
    
    @IBAction func takeOrder(sender:UIButton) {
        expandCell = NSMutableArray()
        for _ in shopDetails {
            expandCell.addObject(false)
        }
        expandCell.replaceObjectAtIndex(sender.tag, withObject: true)
        shopListTable.reloadData()
    }
    
    @IBAction func previewOrder(sender:UIButton) {
        if sender.titleLabel?.text == "Preview" {
            var dummyOrderList = [OrderModel]()
            dummyOrderList = orderListModel
            if orderListModel.count != 0 {
                
                var idx = 0
                var totalOrderPrice:Float = 0
                var actualOrderPrice:Float = 0
                var actualPriceFloat:Float = 0
                var taxAdded:Float = 0
                var discountAdded:Float = 0
                
                
                let shopDetail = self.shopDetails[sender.tag]
                let orderid = self.generateOrderId(shopDetail)
                
                var newOrderModel = [OrderModel]()
                
                for order in dummyOrderList {
                    if order.productQty != "0" {
                        let price = Float(dummyOrderList[idx].productPrice)
                        let qty = Float(dummyOrderList[idx].productQty)
                        let tax = Float(dummyOrderList[idx].productTax)
                        let disc = Float(dummyOrderList[idx].productDisc)
                        
                        let taxForOneProduct = (price!/100) * tax!
                        let actualTax = taxForOneProduct * qty!
                        
                        let discForOneProduct = (price!/100) * disc!
                        let actualDisc = discForOneProduct * qty!
                        
                        let productPrice = ((price! * qty!) + actualTax) - actualDisc
                        
                        
                        actualOrderPrice += price!
                        totalOrderPrice += productPrice
                        actualPriceFloat += price! * qty!
                        taxAdded += actualTax
                        discountAdded += actualDisc
                        
                        dummyOrderList[idx].productTotalPrice = String(format: "%.2f", productPrice)
                        
                        newOrderModel.append(dummyOrderList[idx])
                    }
                    idx += 1
                }
                if newOrderModel.count == 0 {
                    HelperClass.showSimpleAlertWith("Unable to preview this order, \nPlease try again after selecting products", inVC: self)
                    return
                }
                let date = NSDate()
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateStyle = .MediumStyle
                dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
                let strDate = dateFormatter.stringFromDate(date)
                
                let i = sender.tag
                self.orderDetailsModel[i].shopName = shopDetail.shopName
                self.orderDetailsModel[i].shopOwner = shopDetail.customerName
                self.orderDetailsModel[i].shopPhone = shopDetail.customerPhone
                self.orderDetailsModel[i].shopArea = shopDetail.shopArea
                self.orderDetailsModel[i].orderDate = strDate
                self.orderDetailsModel[i].orderQty = String(newOrderModel.count)
                self.orderDetailsModel[i].orderId = orderid
                self.orderDetailsModel[i].orderPrice = String(format: "%.2f", totalOrderPrice)
                self.orderDetailsModel[i].orderStatus = OrderStatus.NoOrder
                self.orderDetailsModel[i].pendingAmount = String(format: "%.2f", totalOrderPrice)
                self.orderDetailsModel[i].orderTax = String(format: "%.2f", taxAdded)
                self.orderDetailsModel[i].orderDiscount = String(discountAdded)
                self.orderDetailsModel[i].shoplatLng = shopDetail.shopLatLng
                
                previewOrderDetailModel = self.orderDetailsModel[i]
                self.title = previewOrderDetailModel.shopName
                previewOrderList = newOrderModel
                previewCustomerModel = shopDetail
                
                actualPrice.text = String(format: "%.2f", actualOrderPrice)
                tax.text = "+\(orderDetailsModel[i].orderTax) ₹"
                discount.text = "-\(orderDetailsModel[i].orderDiscount) ₹"
                orderPrice.text = "\(orderDetailsModel[i].orderPrice) ₹"
                
                previwContainer.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64)
                self.view.addSubview(previwContainer)

                previewTable.reloadData()
                
            }
            
        }
        
    }
    
    @IBAction func savePreviewedOrder(sender:UIButton) {
        let alert = UIAlertController(title: nil, message: "Are you sure to place an order ?", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "YES", style: .Default, handler: { (action) in
            self.previewOrderDetailModel.orderStatus =  OrderStatus.OrderTaken
            let orderid = self.previewOrderDetailModel.orderId
            var itemRef = self.orderDetailRef.child(orderid)
//            itemRef.setValue(self.previewOrderDetailModel.toAnyObject())
            
            itemRef.setValue(self.previewOrderDetailModel.toAnyObject(), withCompletionBlock: { (error, ref) in
                if error == nil {
                    for list in self.previewOrderList {
                        itemRef = self.orderListRef.child(orderid)
                        itemRef = itemRef.child(list.productName)
                        itemRef.setValue(list.toAnyObject())
                        itemRef.setValue(list.toAnyObject(), withCompletionBlock: { (error, ref) in
                            if error == nil {
                                dispatch_async(dispatch_get_main_queue(), { 
                                    let alert = UIAlertController(title: nil, message: "Order Successfully Placed", preferredStyle: .Alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                                        self.previwContainer.removeFromSuperview()
                                        var id = 0
                                        for cell in self.expandCell {
                                            self.loadStockList()
                                            if cell as! Bool {
                                                self.expandCell.replaceObjectAtIndex(id, withObject: false)
                                            }
                                            id += 1
                                        }
                                        
                                        self.shopListTable.reloadData()
                                    }))
                                    self.navigationController?.presentViewController(alert, animated: true, completion: nil)
                                })
                                
                            }
                        })
                    }
                }
            })
            
            //Add new order to customer Table
            var orderList = self.previewCustomerModel.orderList
            if orderList == nil {
                orderList = [String]()
            }
            orderList?.append(orderid)
            self.previewCustomerModel.ref?.updateChildValues([
                "order_list": orderList!
                ])
            
            var previewStockNames = [String]()
            for stock in self.previewOrderList {
                previewStockNames.append(stock.productName)
            }
            for stock in self.stockDetails {
                if previewStockNames.contains(stock.name) {
                    let pIndex = previewStockNames.indexOf(stock.name)
                    let sIndex = self.stockNames.indexOf(stock.name)
                    
                    let pOrderModel = self.previewOrderList[pIndex!]
                    var sStockModel = self.stockDetails[sIndex!]

                    sStockModel.Qty = String(Int(sStockModel.Qty)! - Int(pOrderModel.productQty)!)
                    sStockModel.ref?.updateChildValues(["item_qty":sStockModel.Qty])

                }
            }

        }))
        alert.addAction(UIAlertAction(title: "NO", style: .Cancel, handler: nil))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)

    }
    
    @IBAction func cancelPreview(sender:UIButton) {
        self.title = previewOrderDetailModel.shopArea
        previwContainer.removeFromSuperview()
    }
    
    @IBAction func noOrder(sender:UIButton) {
        expandCell.replaceObjectAtIndex(sender.tag, withObject: false)
        loadStockList()
        shopListTable.reloadData()
    }
    
    @IBAction func done(sender:UIButton) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    //MARK: Support:
    func even(number: Int) -> Bool {
        // Return true if number is evenly divisible by 2.
        return number % 2 == 0
    }
    
    func generateOrderId(customer:CustomerModel) -> String {
        
        let date = NSDate()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.dateFormat = "yyyy/MM/dd/hh/mm"
        var strDate = dateFormatter.stringFromDate(date)
        strDate.append(customer.customerName.characters.first!)
        strDate.append(customer.shopName.characters.first!)
        strDate.append(customer.customerPhone.characters.first!)
        strDate.append(customer.customerPhone.characters.last!)
        strDate.append(customer.shopArea.characters.first!)
        strDate.append(customer.shopArea.characters.last!)
        strDate.uppercaseString
        let newStr = strDate.stringByReplacingOccurrencesOfString("/", withString: "")
        return newStr
    }
    //MARK: ScrollViewDelegate:
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    //MARK: TextfieldDelegate:
    func textFieldDidBeginEditing(textField: UITextField) {
        let section = expandCell.indexOfObject(true)
        let indexPath = NSIndexPath(forRow: textField.tag, inSection:section)
        let cell = self.shopListTable.cellForRowAtIndexPath(indexPath) as! TakeOrderCellTwo
        
        
            
        cell.indicatorImage.image = UIImage(named: "Okay")
        if textField.text == "0" {
            orderListModel[indexPath.row - 1].productQty = "1"
        }
        orderListModel[indexPath.row - 1].productQty = textField.text!
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let section = expandCell.indexOfObject(true)
        let indexPath = NSIndexPath(forRow: textField.tag, inSection:section)
        
        if textField.text == "0" {
            orderListModel[indexPath.row - 1].productQty = "0"
        }else{
            orderListModel[indexPath.row - 1].productQty = "\(textField.text!)\(string)"
        }
        return true
    }
    
}


class TakeOrderCellOne: UITableViewCell {
    @IBOutlet var takeOrder:UIButton!
    @IBOutlet var previewOrder:UIButton!
    @IBOutlet var noOrder:UIButton!
    @IBOutlet var shopName:UILabel!
    @IBOutlet var cancelWidthConstraint:NSLayoutConstraint!
}

class TakeOrderCellTwo: UITableViewCell {
    @IBOutlet var productName:UILabel!
    @IBOutlet var stockQty:UILabel!
    @IBOutlet var stockPrice:UILabel!
    @IBOutlet var orderStepper:UIStepper!
    @IBOutlet var orderQty:UITextField!
    @IBOutlet var indicatorImage:UIImageView!
}


class PreviewTableCell: UITableViewCell {
    @IBOutlet var sNo:UILabel!
    @IBOutlet var productName:UILabel!
    @IBOutlet var productQty:UILabel!
    @IBOutlet var productPrice:UILabel!
    @IBOutlet var productTax:UILabel!
    @IBOutlet var productDisc:UILabel!
    @IBOutlet var productTotalPrice:UILabel!
}