//
//  Driver.swift
//  Shopper
//
//  Created by VividMacmini7 on 05/04/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class Driver:UIViewController,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource, UISearchResultsUpdating  {
    
    @IBOutlet var driverListTable:UITableView!
    
    let driverRef = FIRDatabase.database().referenceWithPath("driver_list")
    let areaRef = FIRDatabase.database().referenceWithPath("area_list")
    
    var driverList = [DriverModel]()
    var filteredDriverList = [DriverModel]()
    var searchController:UISearchController!
    var isFiltered = false
    var selectedScopeIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        driverListTable.estimatedRowHeight = 75
        driverListTable.tableFooterView = UIView()
        
        searchController = UISearchController(searchResultsController: nil)
        driverListTable.tableHeaderView = searchController.searchBar
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Driver"
        searchController.searchBar.delegate = self
        searchController.searchBar.scopeButtonTitles = ["Name", "Phone Number"]
    }
    
    override func viewWillAppear(animated: Bool) {
        driverListTable.userInteractionEnabled = false
        driverList = [DriverModel]()
        driverRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let stock = DriverModel(snapshot: child as! FIRDataSnapshot)
                    self.driverList.append(stock)
                }
                dispatch_async(dispatch_get_main_queue(), {
                    if self.driverList.count == 0 {
                        let backgroundImage = UIImage(named: "TableBG1")
                        let imageView = UIImageView(image: backgroundImage)
                        self.driverListTable.backgroundView = imageView
                    }
                    self.driverListTable.userInteractionEnabled = true
                    self.driverListTable.reloadData()
                })
                
                self.title = "Driver (\(self.driverList.count))"
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func addNewDriver(sender: UIButton) {
        let vc:AddSalesman = self.storyboard?.instantiateViewControllerWithIdentifier("AddSalesman") as! AddSalesman
        vc.isAddDriver = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: UISearchBarDelegate
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        isFiltered = false
        driverListTable.reloadData()
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filterContentForSearchText(searchText)
            driverListTable.reloadData()
        }
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchBar.text = ""
        selectedScopeIndex = selectedScope
    }
    
    func filterContentForSearchText(searchText: String) {
        if searchText.characters.count == 0 {
            isFiltered = false
            driverListTable.reloadData()
            return
        }
        var searchContent = [String]()
        if selectedScopeIndex == 0 {
            for customerDetail in driverList {
                searchContent.append(customerDetail.driverName)
            }
        }else if selectedScopeIndex == 1 {
            for customerDetail in driverList {
                searchContent.append(customerDetail.driverPhone)
            }
        }
        
        
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
        let array = (searchContent as NSArray).filteredArrayUsingPredicate(searchPredicate)
        
        
        let mutableArray = array as NSArray
        filteredDriverList = [DriverModel]()
        var i = 0;
        for customer in searchContent {
            if mutableArray.containsObject(customer)  {
                filteredDriverList.append(driverList[i])
            }
            i += 1
        }
        isFiltered = true
        driverListTable.reloadData()
    }
    
    
    
    //MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? filteredDriverList.count : driverList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:SalesmanListCell = tableView.dequeueReusableCellWithIdentifier("DriverListCell", forIndexPath: indexPath) as! SalesmanListCell
        let item = isFiltered ? filteredDriverList[indexPath.row] : driverList[indexPath.row]
        
        cell.salesmanName.text = item.driverName
        cell.salesmanAddress.text = item.driverAddress
        cell.salesmanPhone.text = item.driverPhone
        let name = item.driverName
        let seprator = name .componentsSeparatedByString(" ")
        let characterOne = seprator[0].characters
        if seprator.count > 1 {
            let characterTwo = seprator[1].characters.count != 0 ? seprator[1].characters : characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(characterTwo.first!)"
            cell.avatar.text = avatorText.uppercaseString
        }else{
            let secondChar = characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(secondChar.first!)"
            cell.avatar.text = avatorText.uppercaseString
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("DriverProfile") as! DriverProfile
        vc.driverDetails = isFiltered ? filteredDriverList[indexPath.row] : driverList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let driver = isFiltered ? filteredDriverList[indexPath.row] : driverList[indexPath.row]
            print(driver.driverEmail)
            deleteUserAccount(driver)
            driver.ref?.removeValue()
            driverList.removeAtIndex(indexPath.row)
            updateAreaList(driver.driverName)
            self.title = "Salesman (\(self.driverList.count))"
            self.driverListTable.reloadData()
        }
    }
    
    //MARK: Support:
    func deleteUserAccount(driver:DriverModel) {
        
        print(driver.driverEmail)
        FIRAuth.auth()?.signInWithEmail(driver.driverEmail, password: driver.driverPassword, completion: { (user, error) in
            print(user?.email)
            user?.deleteWithCompletion({ (error) in
                if error != nil {
                    // An error happened.
                } else {
                    let alertController = UIAlertController(title: nil, message: "Driver deleted successfully", preferredStyle: .Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    // Account deleted.
                }
            })
        })
//        FIRAuth.auth()?.signInWithEmail(driver.driverEmail, password: driver.driverPassword, completion: nil)
        
    }
    
    func updateAreaList(driverName:String) {
        var areaList = [AreaModule]()
        areaRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let area = AreaModule(snapshot: child as! FIRDataSnapshot)
                    areaList.append(area)
                }
                for area in areaList {
                    if area.driver == driverName {
                        area.ref?.updateChildValues([
                            "driver": "",
                            "driver_id": ""
                            ])
                    }
                }
            }
        })
        
        
    }
}
