//
//  AddSalesman.swift
//  Shopper
//
//  Created by VividMacmini7 on 28/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class AddSalesman: UITableViewController,UITextFieldDelegate {
    
    @IBOutlet var salesmanName:UITextField!
    @IBOutlet var salesmanPhone:UITextField!
    @IBOutlet var salesmanAddress:UITextField!
    @IBOutlet var salesmanEmail:UITextField!
    @IBOutlet var salesmanPassword:UITextField!
    @IBOutlet var salesmanWorkArea:UITextView!
    
    var salesmanDetails:SalesmanModel!
    var driverDetails:DriverModel!
    
    var otherMansAreaSelected = false
    var isAddDriver = false
    
    let driverRef = FIRDatabase.database().referenceWithPath("driver_list")
    let ref = FIRDatabase.database().referenceWithPath("salesman_list")
    let areaReferenc = FIRDatabase.database().referenceWithPath("area_list")
    let userReferenc = FIRDatabase.database().referenceWithPath("user_list")
    
    var selectedWorkArea = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isAddDriver {
            salesmanName.placeholder = "Driver Name"
            self.title = "Driver Signup"
            if driverDetails != nil {
                self.title = "Edit profile"
                salesmanName.text = driverDetails.driverName
                salesmanPhone.text = driverDetails.driverPhone
                salesmanAddress.text = driverDetails.driverAddress
                salesmanEmail.text = driverDetails.driverEmail
                salesmanPassword.text = driverDetails.driverPassword
                if driverDetails.workArea != nil {
                    let workArea:AnyObject = driverDetails.workArea!
                    selectedWorkArea = driverDetails.workArea!
                    salesmanWorkArea.text = workArea.componentsJoinedByString(", \n")
                    salesmanWorkArea.resignFirstResponder()
                }else{
                    salesmanWorkArea.text = "--Select Area--"
                }
                
                salesmanEmail.enabled = false
                salesmanPassword.enabled = false
                salesmanEmail.textColor = UIColor.darkGrayColor()
                salesmanPassword.textColor = UIColor.darkGrayColor()
                
            }
        }else{
            if salesmanDetails != nil {
                self.title = "Edit profile"
                salesmanName.text = salesmanDetails.salesmanName
                salesmanPhone.text = salesmanDetails.salesmanPhone
                salesmanAddress.text = salesmanDetails.salesmanAddress
                salesmanEmail.text = salesmanDetails.salesmanEmail
                salesmanPassword.text = salesmanDetails.salesmanPassword
                if salesmanDetails.workArea != nil {
                    let workArea:AnyObject = salesmanDetails.workArea!
                    selectedWorkArea = salesmanDetails.workArea!
                    salesmanWorkArea.text = workArea.componentsJoinedByString(", \n")
                    salesmanWorkArea.resignFirstResponder()
                }else{
                    salesmanWorkArea.text = "--Select Area--"
                }
                
                salesmanEmail.enabled = false
                salesmanPassword.enabled = false
                salesmanEmail.textColor = UIColor.darkGrayColor()
                salesmanPassword.textColor = UIColor.darkGrayColor()
                
            }
        }
        
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(areaSelected(_:)), name: "keyWorkAreaSelected", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(otherMansArea(_:)), name: "keySelectedOtherSalesmansArea", object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: UITableviewDataSource:
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 5 {
            return UITableViewAutomaticDimension
        }
        return 75
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.view.endEditing(true)
    }
    
    //MARK: UITextFieldDelegate:
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 15 {
            let numbersOnly = NSCharacterSet(charactersInString: "1234567890")
            let characterSetFromTextField = NSCharacterSet(charactersInString: string)
            
            let Validate:Bool = numbersOnly .isSupersetOfSet(characterSetFromTextField)
            if !Validate {
                return false;
            }
            if range.length + range.location > textField.text?.characters.count {
                return false
            }
            let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
            
            return newLength <= 11;
        }
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField.tag == 18 {
            self.tableView.contentOffset = CGPointMake(0, 100)
        }
    }
    
    //MARK: IBAction
    @IBAction func selectArea(sender: UIButton) {
        let vc:AreaList = self.storyboard?.instantiateViewControllerWithIdentifier("AreaList") as! AreaList
        vc.isSelectDriverArea = isAddDriver
        if  salesmanDetails != nil {
            vc.selecteduserProfile = salesmanDetails.salesmanName
        }else if driverDetails != nil {
            vc.selecteduserProfile = driverDetails.driverName
        }
        self.navigationController?.pushViewController(vc, animated: true)
        self.view.endEditing(true)
    }
    
    @IBAction func cancel(sender: UIButton) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func save(sender: UIButton) {
        
        if validateFields() {
            let alertController = UIAlertController(title: nil, message: "Please enter all fields", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            if isAddDriver {
                if self.driverDetails != nil { //****  Edit Driver Profile ***//
                    print(selectedWorkArea)
                    driverDetails.ref?.updateChildValues([
                        "driver_name": salesmanName.text!,
                        "driver_phone": salesmanPhone.text! ,
                        "driver_address": salesmanAddress.text! ,
                        "work_area": selectedWorkArea ,
                        ])
                    self.updateAreaList(driverDetails.key)
                    self.updateUserModel(driverDetails.key)
                    checkWithOtherDriverWorkArea()
                    let alert = UIAlertController(title: nil, message: "\(driverDetails.driverName)'s profile edited successfully", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: { (action) in
                        self.navigationController!.popViewControllerAnimated(true)
                        
                    }))
                    self.navigationController?.presentViewController(alert, animated: true, completion: nil)
                    
                }else{  //****  Add Driver Profile ****//
                    
                    FIRAuth.auth()?.createUserWithEmail(salesmanEmail.text!, password: salesmanPassword.text!, completion: { (user, error) in
                        if error == nil {
                            self.updateAreaList((user?.uid)!)
                            self.storeDriverDetails((user?.uid)!)
                            
                            let userModel = UserModel(id: (user?.uid)!, name: self.salesmanName.text!, phone: self.salesmanPhone.text!, email: self.salesmanAddress.text!, type: .UserTypeDriver)
                            
                            let itemRef = self.userReferenc.child((user!.uid))
                            itemRef.setValue(userModel.toAnyObject())
                            
                            
                        } else {
                            let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .Alert)
                            
                            let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                            alertController.addAction(defaultAction)
                            self.presentViewController(alertController, animated: true, completion: nil)
                        }
                    })
                    
                }
            }else{
                if self.salesmanDetails != nil { //**** Edit Salesman Profile ****//
                    print(selectedWorkArea)
                    salesmanDetails.ref?.updateChildValues([
                        "salesman_name": salesmanName.text!,
                        "salesman_phone": salesmanPhone.text! ,
                        "salesman_address": salesmanAddress.text! ,
                        "work_area": selectedWorkArea ,
                        ])
                    self.updateAreaList(salesmanDetails.key)
                    self.updateUserModel(driverDetails.key)
                    
                    checkWithOtherSalesmanWorkArea()
                    let alert = UIAlertController(title: nil, message: "\(salesmanDetails.salesmanName) profile edited successfully", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: { (action) in
                        self.navigationController!.popViewControllerAnimated(true)
                        
                    }))
                    self.navigationController?.presentViewController(alert, animated: true, completion: nil)
                    
                }else{  //**** Add Salesman Profile ****//
                    
                    FIRAuth.auth()?.createUserWithEmail(salesmanEmail.text!, password: salesmanPassword.text!, completion: { (user, error) in
                        if error == nil {
                            self.updateAreaList((user?.uid)!)
                            self.storeSalesmanDetails((user?.uid)!)
                            
                            let userModel = UserModel(id: (user?.uid)!, name: self.salesmanName.text!, phone: self.salesmanPhone.text!, email: self.salesmanAddress.text!, type: .UserTypeSalesman)
                            
                            let itemRef = self.userReferenc.child((user!.uid))
                            itemRef.setValue(userModel.toAnyObject())
                            
                        } else {
                            let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .Alert)
                            
                            let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                            alertController.addAction(defaultAction)
                            self.presentViewController(alertController, animated: true, completion: nil)
                        }
                    })
                    
                }
            }
            
        }
    }
    
    //MARK: Support:
    

    func checkWithOtherSalesmanWorkArea() {
        ref.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let salesman = SalesmanModel(snapshot: child as! FIRDataSnapshot)
                    if salesman.key != self.salesmanDetails.key && salesman.workArea != nil { //Remove current user from validation
                        for idx in self.selectedWorkArea {
                            var workAreas = salesman.workArea!
                            if workAreas.contains(idx) {
                                let index = workAreas.indexOf(idx)
                                workAreas.removeAtIndex(index!)
                                salesman.ref?.updateChildValues([
                                    "work_area":workAreas
                                    ])
                            }
                        }
                        
                    }

                }
            }
        })
    }
    
    
    func checkWithOtherDriverWorkArea() {
        driverRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let driver = DriverModel(snapshot: child as! FIRDataSnapshot)
                    if driver.key != self.driverDetails.key && driver.workArea != nil { //Remove current user from validation
                        for idx in self.selectedWorkArea {
                            var workAreas = driver.workArea!
                            if workAreas.contains(idx) {
                                let index = workAreas.indexOf(idx)
                                workAreas.removeAtIndex(index!)
                                driver.ref?.updateChildValues([
                                    "work_area":workAreas
                                    ])
                            }
                        }
                        
                    }
                    
                }
            }
        })
    }
    
    
    func areaSelected(sender:NSNotification) {
        let userInfo = sender.userInfo
        let selectedArea = userInfo!["area"]
        selectedWorkArea = selectedArea as! [String]
        if selectedWorkArea.count == 0 {
            salesmanWorkArea.text = "--Select Area--"
        }else{
            salesmanWorkArea.text = selectedArea?.componentsJoinedByString(", \n")
        }
        salesmanWorkArea.resignFirstResponder()
        tableView.reloadData()
    }
    
    func otherMansArea(sender:NSNotification) {
        let userInfo = sender.userInfo
        let selectedArea = userInfo!["check"]
        let checks = selectedArea as! [Bool]
        if checks[0] {
            otherMansAreaSelected = true
        }else{
            otherMansAreaSelected = false
        }
        
    }
    
    func updateUserModel(uid:String) {
        userReferenc.observeSingleEventOfType(.Value, withBlock: { snapshot in
            if !(snapshot.value is NSNull) {
                for snap in snapshot.children {
                    let user = UserModel(snapshot: snap as! FIRDataSnapshot)
                    if user.uId == uid {
                        user.ref?.updateChildValues([
                            "u_name" : self.salesmanName.text! ,
                            "u_phone" : self.salesmanPhone.text! ,
                            ])
                    }
                }
            }
        })
    }
    
    func updateAreaList(uid:String) {
        var areaList = [AreaModule]()
        areaReferenc.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let area = AreaModule(snapshot: child as! FIRDataSnapshot)
                    areaList.append(area)
                }
                if self.isAddDriver { //Update Driver name in area list
                    for area in areaList {
                        if self.selectedWorkArea.contains(area.areaName) { //Driver has been added to an area
                            area.ref?.updateChildValues([
                                "driver": self.salesmanName.text!,
                                "driver_id": uid
                                ])
                        }

                        if self.driverDetails != nil {
                            if area.driver == self.driverDetails.driverName &&  !self.selectedWorkArea.contains(area.areaName){  //Driver has been removed from an area
                                area.ref?.updateChildValues([
                                    "driver": "",
                                    "driver_id": ""
                                    ])
                            }
                        }
                        
                    }
                }else{ //Update Salesman name in area list
                    for area in areaList {
                        if self.selectedWorkArea.contains(area.areaName) { //Salesman has been added to an area
                            area.ref?.updateChildValues([
                                "salesman": self.salesmanName.text!,
                                "salesman_id": uid
                                ])
                        }
                        if self.salesmanDetails != nil {
                            if area.salesman == self.salesmanDetails.salesmanName &&  !self.selectedWorkArea.contains(area.areaName){  //Salesman has been removed from an area
                                area.ref?.updateChildValues([
                                    "salesman": "",
                                    "salesman_id": ""
                                    ])
                            }
                        }
                        
                    }
                }
                
            }
        })
    }

    
    func storeSalesmanDetails(uid:String) {
        
        let salesmanDetails = SalesmanModel(name: salesmanName.text!, phone: salesmanPhone.text!, address: salesmanAddress.text!, email: salesmanEmail.text!, password: salesmanPassword.text!, workareas: selectedWorkArea)
        
        let itemRef = self.ref.child(uid)
        itemRef.setValue(salesmanDetails.toAnyObject())
        
        let alert = UIAlertController(title: nil, message: "Salesman Added Successfully", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: { (action) in
            self.navigationController!.popViewControllerAnimated(true)
            
        }))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func storeDriverDetails(uid:String) {
        
        let driverDetails = DriverModel(name: salesmanName.text!, phone: salesmanPhone.text!, address: salesmanAddress.text!, email: salesmanEmail.text!, password: salesmanPassword.text!, workareas: selectedWorkArea)
        
        let itemRef = self.driverRef.child(uid)
        itemRef.setValue(driverDetails.toAnyObject())
        
        let alert = UIAlertController(title: nil, message: "Driver Added Successfully", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: { (action) in
            self.navigationController!.popViewControllerAnimated(true)
            
        }))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func validateFields() ->Bool {
        if salesmanName.text == "" || salesmanPhone.text == "" || salesmanEmail.text == "" || salesmanPassword.text == "" || salesmanAddress.text == "" || selectedWorkArea.count == 0{
            return true
        }
        return false
    }
}
