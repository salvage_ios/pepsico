//
//  EditOrder.swift
//  Shopper
//
//  Created by VividMacmini7 on 03/04/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class EditOrder: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    //MARK: IBOutlet:
    @IBOutlet var shopListTable:UITableView!
    
    //MARK: Variable:
    var stockDetails = [StockItem]()
    var orderListModel = [OrderModel]()
    var oldOrderListModel = [OrderModel]()
    var oldOrderDetail:OrderDetailModel!
    var oldStockName = [String]()
    var allStockName = [String]()
    
    let stockRef = FIRDatabase.database().referenceWithPath("stock_items")
    var orderListRef = FIRDatabase.database().referenceWithPath("order_list")
    var orderDetailRef = FIRDatabase.database().referenceWithPath("order_Details")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.orderListModel = [OrderModel]()
        stockRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            if !(snapshot.value is NSNull) {
                for child in snapshot.children {
                    let stock = StockItem(snapshot: child as! FIRDataSnapshot)
                    self.stockDetails.append(stock)
                    
                    let order = OrderModel(productName: stock.name, productPrice: stock.price, productQty: "0", productTotlaPrice: "0", productTax: stock.tax, productDisc: stock.discount)
                    self.orderListModel.append(order)
                }
               self.oldOrderStockName()
            }
        })
 

 
    }
 
    func oldOrderStockName() {
        oldStockName = [String]()
        for order in oldOrderListModel {
            oldStockName.append(order.productName)
        }
        for order in orderListModel {
            allStockName.append(order.productName)
        }
        updateOrderListWithOldValue()
    }
    func updateOrderListWithOldValue() {
        for order in orderListModel {
            if oldStockName.contains(order.productName) {
                let index = allStockName.indexOf(order.productName)
                let oldIndex = oldStockName.indexOf(order.productName)
                self.orderListModel.removeAtIndex(index!)
                let order = OrderModel(productName: self.oldOrderListModel[oldIndex!].productName, productPrice: self.oldOrderListModel[oldIndex!].productPrice, productQty: self.oldOrderListModel[oldIndex!].productQty, productTotlaPrice: self.oldOrderListModel[oldIndex!].productTotalPrice, productTax: self.oldOrderListModel[oldIndex!].productTax, productDisc: self.oldOrderListModel[oldIndex!].productDisc)
                self.orderListModel.insert(order, atIndex: index!)
            }
        }
        dispatch_async(dispatch_get_main_queue()) { 
            self.shopListTable.reloadData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: UItableView DataSource,Delegate:
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stockDetails.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("EditOrderCell", forIndexPath: indexPath) as! TakeOrderCellTwo
        cell.productName.text = stockDetails[indexPath.row].name
        
        let myAttribute = [NSFontAttributeName: UIFont(name: "TamilSangamMN", size: 15)!]
        
        //Price
        let str = "Price: \(stockDetails[indexPath.row].price) ₹"
        var attributeStr = NSMutableAttributedString(string: str, attributes: myAttribute )
        attributeStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: NSRange(location: 0, length: 5))
        attributeStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.blackColor() , range: NSRange(location: 6, length: "\(stockDetails[indexPath.row].price) ₹".characters.count))
        cell.stockPrice.attributedText = attributeStr
        
        //Qty
        attributeStr = NSMutableAttributedString(string: "Qty: \(stockDetails[indexPath.row].Qty)", attributes: myAttribute )
        attributeStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: NSRange(location: 0, length: 3))
        attributeStr.addAttribute(NSForegroundColorAttributeName, value: UIColor.blackColor() , range: NSRange(location: 4, length: stockDetails[indexPath.row].Qty.characters.count + 1))
        cell.stockQty.attributedText = attributeStr
        
        cell.orderStepper.addTarget(self, action: #selector(stepperAction(_:)), forControlEvents: .ValueChanged)
        cell.orderStepper.minimumValue = 0
        cell.orderStepper.value = Double(orderListModel[indexPath.row].productQty)!
        cell.orderStepper.tag = indexPath.row
        cell.orderQty.tag = indexPath.row
        cell.orderQty.delegate = self

        let orderDetail = orderListModel[indexPath.row]
        cell.orderQty.text = orderDetail.productQty
        cell.indicatorImage.tintColor = UIColor.appThemeGreen()
        cell.indicatorImage.image = orderDetail.productQty != "0" ? UIImage(named: "Okay") : UIImage(named: "NotOkay")
        cell.backgroundColor = orderDetail.productQty != "0" ? UIColor.selectedStockBG() : UIColor.whiteColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
   
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.view.endEditing(true)
        if indexPath.row != 0 {
            if orderListModel[indexPath.row].productQty != "0" {
                orderListModel[indexPath.row].productQty = "0"
            }else if orderListModel[indexPath.row].productQty == "0" {
                orderListModel[indexPath.row].productQty = "1"
            }
            
            shopListTable.reloadData()
        }
    }
    
    //MARK: IBAction:
    @IBAction func stepperAction(sender:UIStepper) {
        
        self.view.endEditing(true)
        let indexPath = NSIndexPath(forRow: sender.tag, inSection:0)
        let stepperOldValue = Double(orderListModel[indexPath.row].productQty)
        let cell = self.shopListTable.cellForRowAtIndexPath(indexPath) as! TakeOrderCellTwo
        var oldValue:Int = Int(cell.orderQty.text!)!
        if sender.value > stepperOldValue {
            oldValue += 1
        }else{
            oldValue -= 1
        }
        
        cell.orderQty.text = oldValue < 0 ? "0" : String(oldValue)
        orderListModel[indexPath.row].productQty = oldValue < 0 ? "0" : String(oldValue)
        shopListTable.reloadData()
    }
    
    @IBAction func saveChanges(sender:UIButton) {
        let alert = UIAlertController(title: nil, message: "Are you sure, Want to save changes?", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action) in
            var totalOrderPrice:Float = 0
            var idx = 0
            var orderQty = 0
            var newOrderList = [String]()
            
            for order in self.orderListModel {
                if order.productQty != "0" {
                    let price = Float(self.orderListModel[idx].productPrice)
                    let qty = Float(self.orderListModel[idx].productQty)
                    let productPrice = price! * qty!
                    totalOrderPrice += productPrice
                    self.orderListModel[idx].productTotalPrice = String(productPrice)
                    orderQty += 1
                    newOrderList.append(order.productName)
                }
                idx += 1
            }
            
            self.oldOrderDetail.ref?.updateChildValues([
                "order_qty": String(orderQty),
                "order_price": String(totalOrderPrice)
                ])
            
            
            
           
            
            for list in self.orderListModel {
                self.orderListRef = FIRDatabase.database().referenceWithPath("order_list")
                if list.productQty != "0" {
                    self.orderListRef = self.orderListRef.child(self.oldOrderDetail.orderId)
                    self.orderListRef = self.orderListRef.child(list.productName)
                    self.orderListRef.updateChildValues(([
                        "product_name" : list.productName,
                        "product_price" : list.productPrice ,
                        "product_qty": list.productQty,
                        "product_totalPrice": list.productTotalPrice
                        ]))
                    self.showSuccesAlert()
                }else{
                    self.orderListRef =  self.orderListRef.child(self.oldOrderDetail.orderId)
                    self.orderListRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
                        
                        if !(snapshot.value is NSNull) {
                            var orderModelInDb = [String]()
                            for child in snapshot.children {
                                let order = OrderModel(snapshot: child as! FIRDataSnapshot)
                                orderModelInDb.append(order.productName)
                            }
                            
                            if orderModelInDb.contains(list.productName) && !newOrderList.contains(list.productName) {// (ie) This producu have been removed
                                 self.orderListRef = FIRDatabase.database().referenceWithPath("order_list")
                                self.orderListRef = self.orderListRef.child(self.oldOrderDetail.orderId)
                                self.orderListRef = self.orderListRef.child(list.productName)
                                self.orderListRef.removeValueWithCompletionBlock({ (error, ref) in
                                    if error == nil {
                                        self.showSuccesAlert()
                                    }
                                    
                                })
                            }
                        }
                    })
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showSuccesAlert() {
        dispatch_async(dispatch_get_main_queue()) { 
            let alert = UIAlertController(title: nil, message: "Order has been successfully edited", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) in
                self.navigationController?.popToRootViewControllerAnimated(true)
            }))
            self.navigationController?.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK: TextfieldDelegate:
    func textFieldDidBeginEditing(textField: UITextField) {
        let indexPath = NSIndexPath(forRow: textField.tag, inSection:0)
        let cell = self.shopListTable.cellForRowAtIndexPath(indexPath) as! TakeOrderCellTwo
        
        cell.indicatorImage.image = UIImage(named: "Okay")
        if textField.text == "0" {
            orderListModel[indexPath.row].productQty = "1"
        }
        orderListModel[indexPath.row].productQty = textField.text!
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let indexPath = NSIndexPath(forRow: textField.tag, inSection:0)
        
        if textField.text == "0" {
            orderListModel[indexPath.row].productQty = "0"
        }else{
            orderListModel[indexPath.row].productQty = "\(textField.text!)\(string)"
        }
        return true
    }

}
