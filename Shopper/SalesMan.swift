//
//  SalesMan.swift
//  Shopper
//
//  Created by VividMacmini7 on 28/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class SalesMan:UIViewController,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource, UISearchResultsUpdating  {
    
    @IBOutlet var salesmanListTable:UITableView!
    
    let ref = FIRDatabase.database().referenceWithPath("salesman_list")
    let areaRef = FIRDatabase.database().referenceWithPath("area_list")
    
    var salesmanList = [SalesmanModel]()
    var filteredSalesmanList = [SalesmanModel]()
    var searchController:UISearchController!
    var isFiltered = false
    var selectedScopeIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        salesmanListTable.estimatedRowHeight = 75
        salesmanListTable.tableFooterView = UIView()
        
        searchController = UISearchController(searchResultsController: nil)
        salesmanListTable.tableHeaderView = searchController.searchBar
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Salesman"
        searchController.searchBar.delegate = self
        searchController.searchBar.scopeButtonTitles = ["Name", "Phone Number"]
    }
    
    override func viewWillAppear(animated: Bool) {
        salesmanList = [SalesmanModel]()
        ref.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let stock = SalesmanModel(snapshot: child as! FIRDataSnapshot)
                    self.salesmanList.append(stock)
                }
                dispatch_async(dispatch_get_main_queue(), {
                    if self.salesmanList.count == 0 {
                        let backgroundImage = UIImage(named: "TableBG1")
                        let imageView = UIImageView(image: backgroundImage)
                        self.salesmanListTable.backgroundView = imageView
                    }
                    self.salesmanListTable.reloadData()
                })
                
                self.title = "Salesman (\(self.salesmanList.count))"
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func addNewSalesman(sender: UIButton) {
        let vc:AddSalesman = self.storyboard?.instantiateViewControllerWithIdentifier("AddSalesman") as! AddSalesman
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: UISearchBarDelegate
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        isFiltered = false
        salesmanListTable.reloadData()
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filterContentForSearchText(searchText)
            salesmanListTable.reloadData()
        }
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchBar.text = ""
        selectedScopeIndex = selectedScope
    }
    
    func filterContentForSearchText(searchText: String) {
        if searchText.characters.count == 0 {
            isFiltered = false
            salesmanListTable.reloadData()
            return
        }
        var searchContent = [String]()
        if selectedScopeIndex == 0 {
            for customerDetail in salesmanList {
                searchContent.append(customerDetail.salesmanName)
            }
        }else if selectedScopeIndex == 1 {
            for customerDetail in salesmanList {
                searchContent.append(customerDetail.salesmanPhone)
            }
        }
        

        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
        let array = (searchContent as NSArray).filteredArrayUsingPredicate(searchPredicate)
        
        
        let mutableArray = array as NSArray
        filteredSalesmanList = [SalesmanModel]()
        var i = 0;
        for customer in searchContent {
            if mutableArray.containsObject(customer)  {
                filteredSalesmanList.append(salesmanList[i])
            }
            i += 1
        }
        isFiltered = true
        salesmanListTable.reloadData()
    }
    
    
    
    //MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? filteredSalesmanList.count : salesmanList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:SalesmanListCell = tableView.dequeueReusableCellWithIdentifier("SalesmanListCell", forIndexPath: indexPath) as! SalesmanListCell
        let item = isFiltered ? filteredSalesmanList[indexPath.row] : salesmanList[indexPath.row]
        
        cell.salesmanName.text = item.salesmanName
        cell.salesmanAddress.text = item.salesmanAddress
        cell.salesmanPhone.text = item.salesmanPhone
        let name = item.salesmanName
        let seprator = name .componentsSeparatedByString(" ")
        let characterOne = seprator[0].characters
        if seprator.count > 1 {
            let characterTwo = seprator[1].characters.count != 0 ? seprator[1].characters : characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(characterTwo.first!)"
            cell.avatar.text = avatorText.uppercaseString
        }else{
            let secondChar = characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(secondChar.first!)"
            cell.avatar.text = avatorText.uppercaseString
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc:SalesmanProfile = self.storyboard?.instantiateViewControllerWithIdentifier("SalesmanProfile") as! SalesmanProfile
        vc.salesmanDetails = isFiltered ? filteredSalesmanList[indexPath.row] : salesmanList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let salesman = isFiltered ? filteredSalesmanList[indexPath.row] : salesmanList[indexPath.row]
            deleteUserAccount(salesman)
            salesman.ref?.removeValue()
            salesmanList.removeAtIndex(indexPath.row)
            updateAreaList(salesman.salesmanName)
            self.title = "Salesman (\(self.salesmanList.count))"
            self.salesmanListTable.reloadData()
        }
    }
    
    //MARK: Support:
    func deleteUserAccount(salesman:SalesmanModel) {

        print(salesman.salesmanEmail)
        FIRAuth.auth()?.signInWithEmail(salesman.salesmanEmail, password: salesman.salesmanPassword, completion: { (user, error) in
            print(user?.email)
            user?.deleteWithCompletion({ (error) in
                if error != nil {
                    // An error happened.
                } else {
                    let alertController = UIAlertController(title: nil, message: "Salesman deleted successfully", preferredStyle: .Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    // Account deleted.
                }
            })
        })
       
    }
    
    func updateAreaList(salesmanName:String) {
        var areaList = [AreaModule]()
        areaRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let area = AreaModule(snapshot: child as! FIRDataSnapshot)
                    areaList.append(area)
                }
                for area in areaList {
                    if area.salesman == salesmanName {
                        area.ref?.updateChildValues([
                            "salesman": "",
                            "salesman_id": ""
                            ])
                    }
                }
            }
        })
        
        
    }
}

class SalesmanListCell: UITableViewCell {
    @IBOutlet var salesmanName:UILabel!
    @IBOutlet var salesmanPhone:UILabel!
    @IBOutlet var salesmanAddress:UILabel!
    @IBOutlet var avatar:UILabel!
    @IBOutlet var avatarBG:UIView!
    var gl:CAGradientLayer!
    
    override func awakeFromNib() {
        let colorTop = UIColor.darkGrayColor().CGColor
        let colorBottom = UIColor.lightGrayColor().CGColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.0, 1.0]
        
        avatarBG.backgroundColor = UIColor.clearColor()
        gl.frame = CGRectMake(0, 0, 50, 50)
        avatarBG.layer.insertSublayer(gl, atIndex: 0)
    }
    
}


