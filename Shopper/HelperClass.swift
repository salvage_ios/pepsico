//
//  HelperClass.swift
//  Shopper
//
//  Created by VividMacmini7 on 30/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit

enum UserType:Int {
    case UserTypAdmin = 0
    case UserTypeSalesman
    case UserTypeDriver
}

enum OrderStatus:Int {
    case NoOrder = 0
    case OrderTaken
    case OrderPacked
    case OrderDelivered
}

enum PaymentMethod:Int {
    case NotPaid = 0
    case PayWithCash
    case PayWithCheque
}

let loggdInUser:String = "keyLoggedInUser"
let googleMapsApiKey = "AIzaSyBxwtjuJv3yXnlEiwAEedVXPMfRbmjwaEs"

let kMapStyle = "[" +
    "  {" +
    "    \"featureType\": \"all\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#242f3e\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"all\"," +
    "    \"elementType\": \"labels.text.stroke\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"lightness\": -80" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"administrative\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#746855\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"administrative.locality\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#d59563\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"poi\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#d59563\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"poi.park\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#263c3f\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"poi.park\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#6b9a76\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road\"," +
    "    \"elementType\": \"geometry.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#2b3544\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#9ca5b3\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.arterial\"," +
    "    \"elementType\": \"geometry.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#38414e\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.arterial\"," +
    "    \"elementType\": \"geometry.stroke\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#212a37\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.highway\"," +
    "    \"elementType\": \"geometry.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#746855\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.highway\"," +
    "    \"elementType\": \"geometry.stroke\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#1f2835\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.highway\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#f3d19c\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.local\"," +
    "    \"elementType\": \"geometry.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#38414e\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.local\"," +
    "    \"elementType\": \"geometry.stroke\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#212a37\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"transit\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#2f3948\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"transit.station\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#d59563\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"water\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#17263c\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"water\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#515c6d\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"water\"," +
    "    \"elementType\": \"labels.text.stroke\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"lightness\": -20" +
    "      }" +
    "    ]" +
    "  }" +
"]"


class HelperClass: NSObject {

    class func showSimpleAlertWith(message:String,inVC:UIViewController) {
        let simpleAlert = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        simpleAlert.addAction(defaultAction)
        inVC.presentViewController(simpleAlert, animated: true, completion: nil)
    }
    
    class func userDetails() -> UserModel? {
        var uKeys = ""
        let value = NSUserDefaults.standardUserDefaults().valueForKey(loggdInUser)
        if value != nil {
            uKeys = value as! String
            let usrDetails = NSUserDefaults.standardUserDefaults().objectForKey(uKeys)
            if (usrDetails != nil) {
                let user = UserModel(snapShot: usrDetails!)
                return user
                
            }
        }
        return nil
    }
}

extension UIColor {
    class func appTheme() -> UIColor {
        return UIColor(red: 255.0/255.0, green: 58.0/255.0, blue: 23.0/255.0, alpha: 1)
    }
    
    class func appThemeBlue() -> UIColor {
        return UIColor(red: 7.0/255.0, green: 62.0/255.0, blue: 138.0/255.0, alpha: 1)
    }
    
    class func appThemeGreen() -> UIColor {
        return UIColor(red: 63.0/255.0, green: 164.0/255.0, blue: 63.0/255.0, alpha: 1)
    }
    
    class func appThemeYellow() -> UIColor {
        return UIColor(red: 255.0/255.0, green: 128.0/255.0, blue: 0.0/255.0, alpha: 1)
    }
    
    class func selectedStockBG() -> UIColor {
        return UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1)
    }
}


extension UIFont {
    class func AppFontBold() -> UIFont {
        return UIFont(name: "TamilSangamMN-Bold", size: 14)!
    }
    
    class func AppFontRegular() -> UIFont {
        return UIFont(name: "TamilSangamMN", size: 14)!
    }
}
