//
//  AppDelegate.swift
//  Shopper
//
//  Created by VividMacmini7 on 27/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let currencyCode = "inr"
    
    
    override init() {
        super.init()
        FIRApp.configure()
        // not really needed unless you really need it FIRDatabase.database().persistenceEnabled = true
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window?.tintColor = UIColor.appTheme()
        GMSServices.provideAPIKey(googleMapsApiKey)
        
//        UINavigationBar.appearance().barTintColor = UIColor.appThemeBlue()
//        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
//        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
//        UINavigationBar.appearance().translucent = false
//        
//        window?.tintColor = UIColor.appThemeBlue()
        
        let value = NSUserDefaults.standardUserDefaults().valueForKey(loggdInUser)
        if value != nil {
            let user = HelperClass.userDetails()
            if user != nil {
                print("---------UserDetails:-----------  \n\(user!)")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if user!.uType == .UserTypAdmin {
                    let initialViewController = storyboard.instantiateViewControllerWithIdentifier("DashBoard") as! DashBoard
                    let navVC = UINavigationController(rootViewController: initialViewController)
                    self.window?.rootViewController = navVC
                }else if user!.uType == .UserTypeSalesman {
                    
                    let vc = storyboard.instantiateViewControllerWithIdentifier("SalesmanProfile") as! SalesmanProfile
                    let nacvc = UINavigationController(rootViewController: vc)
                    self.window?.rootViewController = nacvc
                    
//                    getSalesmanDetlail(user!.uId)
                    
                }else if user!.uType == .UserTypeDriver {
                    let vc = storyboard.instantiateViewControllerWithIdentifier("DriverProfile") as! DriverProfile
                    let nacvc = UINavigationController(rootViewController: vc)
                    self.window?.rootViewController = nacvc
//                    getSalesmanDetlail(user!.uId)
                    
                }
                self.window?.makeKeyAndVisible()
            }
            
        }
        return true
    }
    
//    func getSalesmanDetlail(uid:String)  {
//        let salesmanRef = FIRDatabase.database().referenceWithPath("salesman_list")
//        salesmanRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
//            
//            if snapshot.value is NSNull {
//                
//            } else {
//                for child in snapshot.children {
//                    let salesman = SalesmanModel(snapshot: child as! FIRDataSnapshot)
//                    if salesman.key == uid {
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let initialViewController = storyboard.instantiateViewControllerWithIdentifier("SalesmanProfile") as! SalesmanProfile
//                        initialViewController.salesmanDetails = salesman
//                        let navVC = UINavigationController(rootViewController: initialViewController)
//                        self.window?.rootViewController = navVC
//                    }
//                }
//            }
//        })
//    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: Custom Methods
    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    
    func getStringValueFormattedAsCurrency(value: String) -> String {
        let numberFormatter = NSNumberFormatter()
        numberFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        numberFormatter.currencyCode = currencyCode
        numberFormatter.maximumFractionDigits = 2
        
        let formattedValue = numberFormatter.stringFromNumber(NSNumberFormatter().numberFromString(value)!)
        return formattedValue!
    }
    
    
    func getDocDir() -> String {
        return NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
    }

}

