//
//  CustomerProfile.swift
//  Shopper
//
//  Created by VividMacmini7 on 03/04/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class CustomerProfile: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var avatar:UILabel!
    @IBOutlet var avatarBG:UIView!
    @IBOutlet var customerName:UILabel!
    @IBOutlet var customerAddress:UILabel!
    @IBOutlet var shopName:UILabel!
    @IBOutlet var orderListTable:UITableView!
    
    var customerDetails:CustomerModel!
    var orderIdList = [String]()
    var orderDetail = [OrderDetailModel]()
    
    var orderDetailRef = FIRDatabase.database().referenceWithPath("order_Details")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let colorTop = UIColor.darkGrayColor().CGColor
        let colorBottom = UIColor.lightGrayColor().CGColor
        
        let gl = CAGradientLayer()
        gl.colors = [colorTop, colorBottom]
        gl.locations = [0.0, 1.0]
        
        avatarBG.backgroundColor = UIColor.clearColor()
        gl.frame = CGRectMake(0, 0, 50, 50)
        avatarBG.layer.insertSublayer(gl, atIndex: 0)
        
        orderListTable.estimatedRowHeight = 100
        orderListTable.tableFooterView = UIView()
        
        
    }

    override func viewWillAppear(animated: Bool) {
        orderDetail = [OrderDetailModel]()
        
        if customerDetails.orderList != nil  {
            orderIdList = customerDetails.orderList!
            getOrderDetails()
        }else{
            let backgroundImage = UIImage(named: "TableBG1")
            let imageView = UIImageView(image: backgroundImage)
            self.orderListTable.backgroundView = imageView
        }
        
        let name = customerDetails.customerName
        let seprator = name .componentsSeparatedByString(" ")
        let characterOne = seprator[0].characters
        if seprator.count > 1 {
            let characterTwo = seprator[1].characters.count != 0 ? seprator[1].characters : characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(characterTwo.first!)"
            avatar.text = avatorText.uppercaseString
        }else{
            let secondChar = characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(secondChar.first!)"
            avatar.text = avatorText.uppercaseString
        }
        
        customerAddress.text = customerDetails.shopAddress
        shopName.text = customerDetails.shopName
        customerName.text = customerDetails.customerName
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        //CustomerOrderCell
    }
    
    //MARK: UTtableView Datasource Delegate:
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderDetail.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         let cell:LatestOrderCell = tableView.dequeueReusableCellWithIdentifier("CustomerOrderCell", forIndexPath: indexPath) as! LatestOrderCell
        cell.shopName.text = orderDetail[indexPath.row].orderTakenBy
        cell.orderDate.text = orderDetail[indexPath.row].orderDate
        cell.orderAmount.text = orderDetail[indexPath.row].pendingAmount != "0" ? "\(orderDetail[indexPath.row].pendingAmount) ₹" : "\(orderDetail[indexPath.row].orderPrice) ₹"
        cell.orderAmount.textColor = orderDetail[indexPath.row].pendingAmount != "0" ? UIColor.redColor() : UIColor.appThemeGreen()
        
        let status = orderDetail[indexPath.row].orderStatus
        if status == .OrderTaken {
            cell.connectorOne.backgroundColor = UIColor.lightGrayColor()
            cell.connectorTwo.backgroundColor = UIColor.lightGrayColor()
            
            cell.orderTaken.backgroundColor = UIColor.appThemeYellow()
            cell.packed.backgroundColor = UIColor.lightGrayColor()
            cell.delivered.backgroundColor = UIColor.lightGrayColor()
            
        }else if status == .OrderPacked {
            cell.connectorOne.backgroundColor = UIColor.appThemeGreen()
            cell.connectorTwo.backgroundColor = UIColor.lightGrayColor()
            
            cell.orderTaken.backgroundColor = UIColor.appThemeGreen()
            cell.packed.backgroundColor = UIColor.appThemeGreen()
            cell.delivered.backgroundColor = UIColor.lightGrayColor()
            
        }else if status == .OrderDelivered {
            cell.connectorOne.backgroundColor = UIColor.appTheme()
            cell.connectorTwo.backgroundColor = UIColor.appTheme()
            
            cell.orderTaken.backgroundColor = UIColor.appTheme()
            cell.packed.backgroundColor = UIColor.appTheme()
            cell.delivered.backgroundColor = UIColor.appTheme()
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("OrderProfile") as! OrderProfile
        vc.orderIdStr = orderIdList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: IBAction:
    @IBAction func editCustomerProfile(sender:UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AddCustomer") as! AddCustomer
        vc.customerDetail = customerDetails
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func callShopOwner(sender:UIBarButtonItem) {
        let url:NSURL = NSURL(string: "tel:\(customerDetails.customerPhone)")!
        UIApplication.sharedApplication().openURL(url)
    }
    
    //MARK: Support:
    func getOrderDetails() {

        var orderModel = [OrderDetailModel]()
        orderDetailRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if !(snapshot.value is NSNull) {
                for child in snapshot.children {
                    let detail = OrderDetailModel(snapshot: child as! FIRDataSnapshot)
                    orderModel.append(detail)
                }
                
                for model in orderModel {
                    if self.orderIdList.contains(model.orderId) {
                        self.orderDetail.append(model)
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), { 
                    self.orderListTable.reloadData()
                })
            }
        })
        
    }
    
    

}
