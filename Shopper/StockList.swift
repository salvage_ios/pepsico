//
//  StockList.swift
//  Shopper
//
//  Created by VividMacmini7 on 27/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class StockList: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchResultsUpdating {

    @IBOutlet var stockListTable:UITableView!
    @IBOutlet var addItemAlert:UIView!
   
    
    var searchController:UISearchController!
    var filteredStockList = [StockItem]()
    var stockList = [StockItem]()
    var isFiltered = false
    
    let ref = FIRDatabase.database().referenceWithPath("stock_items")

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController = UISearchController(searchResultsController: nil)
        stockListTable.tableHeaderView = searchController.searchBar
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Stock Items"
        searchController.searchBar.delegate = self
        
        
        
        loadStockList()
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
    func loadStockList() {
        stockList = [StockItem]()
        ref.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                let backgroundImage = UIImage(named: "TableBG1")
                let imageView = UIImageView(image: backgroundImage)
                self.stockListTable.backgroundView = imageView
            } else {
                self.stockListTable.backgroundView = UIView()
                
                for child in snapshot.children {
                    let stock = StockItem(snapshot: child as! FIRDataSnapshot)
                    self.stockList.append(stock)
                }
                dispatch_async(dispatch_get_main_queue(), {
                    if self.stockList.count == 0 {
                        let backgroundImage = UIImage(named: "TableBG1")
                        let imageView = UIImageView(image: backgroundImage)
                        self.stockListTable.backgroundView = imageView
                        return
                    }
                    self.stockListTable.reloadData()
                    self.title = "Stock Items (\(self.stockList.count))"
                })
                
            }
        })
    }
    //MARK: UISearchBarDelegate
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        isFiltered = false
        stockListTable.reloadData()
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filterContentForSearchText(searchText)
            stockListTable.reloadData()
        }
    }
    
    
    func filterContentForSearchText(searchText: String) {
        if searchText.characters.count == 0 {
            isFiltered = false
            stockListTable.reloadData()
            return
        }
        var searchContent = [String]()
        for stockDetails in stockList {
            searchContent.append(stockDetails.name)
        }
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
        let array = (searchContent as NSArray).filteredArrayUsingPredicate(searchPredicate)
        
        
        let mutableArray = array as NSArray
        filteredStockList = [StockItem]()
        var i = 0;
        for customer in searchContent {
            if mutableArray.containsObject(customer)  {
                filteredStockList.append(stockList[i])
            }
            i += 1
        }
        isFiltered = true
        stockListTable.reloadData()
    }

    
    //MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? filteredStockList.count : stockList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:StockListCell = tableView.dequeueReusableCellWithIdentifier("StockListCell", forIndexPath: indexPath) as! StockListCell
        let item = isFiltered ? filteredStockList[indexPath.row] : stockList[indexPath.row]
        
        let price = Double(item.price)
        let qty = Double(item.Qty)
        

        cell.stockPrice.text = "\(item.price) ₹"
        cell.stockQty.text = "\(item.Qty)"
        cell.stockTax.text = "\(item.tax) %"
        cell.stockDiscount.text = "\(item.discount) %"
        cell.stockNetPrice.text = "\(qty! * price!)"
        cell.stockName.text = item.name
        cell.indexNumber.text = "\(indexPath.row + 1)"
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.view.endEditing(true)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
        }
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let moreRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Edit", handler:{action, indexpath in
            self.editStockItem(self.stockList[indexPath.row])
        });
        moreRowAction.backgroundColor = UIColor(red: 0.298, green: 0.851, blue: 0.3922, alpha: 1.0);
        
        let deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete", handler:{action, indexpath in
            let stock = self.stockList[indexPath.row]
            stock.ref?.removeValue()
            self.stockList.removeAtIndex(indexPath.row)
            self.title = "Stock Items (\(self.stockList.count))"
            self.stockListTable.reloadData()
        });
        
        return [deleteRowAction, moreRowAction];
    }
    
    //MARK: IBAction:
    @IBAction func addStockItem(sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "AddStock ?", message: nil, preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Name"
            textfield.autocapitalizationType = .Words
        }
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Price"
            textfield.keyboardType = .DecimalPad
        }
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Qty"
            textfield.keyboardType = .NumberPad
        }
        
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Tax %"
            textfield.keyboardType = .NumberPad
        }
        
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Discount %"
            textfield.keyboardType = .NumberPad
        }
        alert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action) in
            if (alert.textFields![0].text == nil && alert.textFields![1].text == nil && alert.textFields![02].text == nil) {
                self.showAlert(); return
            }
            let stockItem = StockItem(name: alert.textFields![0].text!, Qty: alert.textFields![2].text!, price: alert.textFields![1].text!, tax: alert.textFields![3].text!, discount: alert.textFields![4].text!)
            let itemRef = self.ref.child(alert.textFields![0].text!.lowercaseString)
            itemRef.setValue(stockItem.toAnyObject())
            dispatch_async(dispatch_get_main_queue(), { 
                self.loadStockList()
            })
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)

    }
    
    func editStockItem(sender:StockItem) {
        let alert = UIAlertController(title: "Edit Stock ?", message: nil, preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Name"
            textfield.text = sender.name
            
        }
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Price"
            textfield.text = sender.price
            textfield.keyboardType = .DecimalPad
        }
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Qty"
            textfield.text = sender.Qty
            textfield.keyboardType = .NumberPad
        }
        
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Tax %"
            textfield.text = sender.tax
            textfield.keyboardType = .NumberPad
        }
        
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Discount %"
            textfield.text = sender.discount
            textfield.keyboardType = .NumberPad
        }
        
        alert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action) in
            if (alert.textFields![0].text == nil && alert.textFields![1].text == nil && alert.textFields![2].text == nil) && alert.textFields![3].text == nil && alert.textFields![4].text == nil {
                self.showAlert(); return
            }
            sender.ref?.updateChildValues([
                "item_name" : alert.textFields![0].text!.lowercaseString ,
                "item_price" : alert.textFields![1].text! ,
                "item_qty" : alert.textFields![2].text! ,
                "item_tax" : alert.textFields![3].text! ,
                "item_discount" : alert.textFields![4].text!
                ])
            dispatch_async(dispatch_get_main_queue(), {
                self.loadStockList()
            })
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showAlert() {
        HelperClass.showSimpleAlertWith("Enter All Fields", inVC: self)
    }
    
    
    //MARK: Support:
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
}

class StockListCell: UITableViewCell {
    @IBOutlet var stockName:UILabel!
    @IBOutlet var stockPrice:UILabel!
    @IBOutlet var stockNetPrice:UILabel!
    @IBOutlet var stockQty:UILabel!
    @IBOutlet var indexNumber:UILabel!
    @IBOutlet var stockTax:UILabel!
    @IBOutlet var stockDiscount:UILabel!
    
    
    override func awakeFromNib() {
        
    }
    
}
