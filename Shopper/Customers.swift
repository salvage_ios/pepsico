//
//  Customers.swift
//  Shopper
//
//  Created by VividMacmini7 on 27/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class Customers: UIViewController,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource, UISearchResultsUpdating {

    @IBOutlet var customerListTable:UITableView!
   
    let ref = FIRDatabase.database().referenceWithPath("customer_list")
    let areaRef = FIRDatabase.database().referenceWithPath("area_list")
    var customerList = [CustomerModel]()
    var sectionHeaders = [String]()
    var filteredCustomerList = [CustomerModel]()
    var searchController:UISearchController!
    var selectedScopeIndex = 0
    var isFiltered = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customerListTable.tableFooterView = UIView()
        
        searchController = UISearchController(searchResultsController: nil)
        customerListTable.tableHeaderView = searchController.searchBar
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Customers"
        searchController.searchBar.delegate = self
        searchController.searchBar.scopeButtonTitles = ["Shop Name", "Customer Name", "Phone Number"]
    }
    
    override func viewWillAppear(animated: Bool) {
        loadCustomerList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadCustomerList() {
        customerList = [CustomerModel]()
        sectionHeaders = [String]()
        ref.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                let backgroundImage = UIImage(named: "TableBG1")
                let imageView = UIImageView(image: backgroundImage)
                self.customerListTable.backgroundView = imageView
                return
            } else {
                self.customerListTable.backgroundView = UIView()
                for child in snapshot.children {
                    let stock = CustomerModel(snapshot: child as! FIRDataSnapshot)
                    self.customerList.append(stock)
                    self.sectionHeaders.append(stock.shopArea)
                }
                dispatch_async(dispatch_get_main_queue(), {
                    self.sectionHeaders = Array(Set(self.sectionHeaders))
                    print(self.sectionHeaders)
                    self.customerListTable.reloadData()
                })
                
                self.title = "Customers (\(self.customerList.count))"
            }
        })
    }
    //MARK: UISearchBarDelegate
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        isFiltered = false
        customerListTable.reloadData()
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            filterContentForSearchText(searchText)
            customerListTable.reloadData()
        }
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        searchBar.text = ""
        selectedScopeIndex = selectedScope
    }
    
    func filterContentForSearchText(searchText: String) {
        if searchText.characters.count == 0 {
            isFiltered = false
            customerListTable.reloadData()
            return
        }
        var searchContent = [String]()
        if selectedScopeIndex == 0 {
            for customerDetail in customerList {
                searchContent.append(customerDetail.shopName)
            }
        }else if selectedScopeIndex == 1 {
            for customerDetail in customerList {
                searchContent.append(customerDetail.customerName)
            }
        }else{
            for customerDetail in customerList {
                searchContent.append(customerDetail.customerPhone)
            }
        }
        
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchText)
        let array = (searchContent as NSArray).filteredArrayUsingPredicate(searchPredicate)
       
        
        let mutableArray = array as NSArray
        filteredCustomerList = [CustomerModel]()
        var i = 0;
        for customer in searchContent {
            if mutableArray.containsObject(customer)  {
                filteredCustomerList.append(customerList[i])
            }
            i += 1
        }
         print("Filter Array: \(filteredCustomerList.count)")
        isFiltered = true
        customerListTable.reloadData()
    }



    //MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sectionHeaders.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = sectionHeaders[section]
        let rowContents = isFiltered ? filteredCustomerList : customerList
        var i = 0
        for content in rowContents {
            if section == content.shopArea {
                i += 1
            }
        }
       return i
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:CustomerListCell = tableView.dequeueReusableCellWithIdentifier("CustomerListCell", forIndexPath: indexPath) as! CustomerListCell
        let cellContents = isFiltered ? filteredCustomerList : customerList
        var item = [CustomerModel]()
        let section = sectionHeaders[indexPath.section]
        for content in cellContents {
            if section == content.shopArea {
                item.append(content)
            }
        }
        cell.shopName.text = item[indexPath.row].shopName
        cell.customerName.text = item[indexPath.row].customerName
        cell.customerPhone.text = item[indexPath.row].customerPhone
        let name = item[indexPath.row].shopName
        let seprator = name .componentsSeparatedByString(" ")
        let characterOne = seprator[0].characters
        if seprator.count > 1 {
            let characterTwo = seprator[1].characters.count != 0 ? seprator[1].characters : characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(characterTwo.first!)"
            cell.avatar.text = avatorText.uppercaseString
        }else{
            let secondChar = characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(secondChar.first!)"
            cell.avatar.text = avatorText.uppercaseString
        }
        return cell
    }
    

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,40))
        headerView.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        let areaName = UILabel(frame:  CGRectMake(15,0,self.view.frame.size.width/2,40))
        areaName.text = sectionHeaders[section]
        areaName.textColor = UIColor.blackColor()
        areaName.font = UIFont.AppFontBold()
        headerView.addSubview(areaName)
        
        let salesMan = UILabel(frame:  CGRectMake((self.view.frame.size.width/2) - 15,0,self.view.frame.size.width/2,40))
        salesMan.text = "NA"
        salesMan.textAlignment = .Right
        salesMan.font = UIFont.AppFontRegular()
        
        areaRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let area = AreaModule(snapshot: child as! FIRDataSnapshot)
                    if self.sectionHeaders[section] == area.areaName {
                        salesMan.text = area.salesman.characters.count != 0 ? area.salesman : "NA"
                    }
                }
            }
        })
        
        headerView.addSubview(salesMan)
        return headerView
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cellContents = isFiltered ? filteredCustomerList : customerList
        var item = [CustomerModel]()
        let section = sectionHeaders[indexPath.section]
        for content in cellContents {
            if section == content.shopArea {
                item.append(content)
            }
        }
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("CustomerProfile") as! CustomerProfile
        vc.customerDetails = item[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
           
            let cellContents = isFiltered ? filteredCustomerList : customerList
            var item = [CustomerModel]()
            let section = sectionHeaders[indexPath.section]
            for content in cellContents {
                if section == content.shopArea {
                    item.append(content)
                }
            }
            item[indexPath.row].ref?.removeValue()
            loadCustomerList()
        }
    }
    
    

}

class CustomerListCell: UITableViewCell {
    @IBOutlet var shopName:UILabel!
    @IBOutlet var customerName:UILabel!
    @IBOutlet var customerPhone:UILabel!
    @IBOutlet var avatar:UILabel!
    @IBOutlet var avatarBG:UIView!
    var gl:CAGradientLayer!
    
    override func awakeFromNib() {
        let colorTop = UIColor.darkGrayColor().CGColor
        let colorBottom = UIColor.lightGrayColor().CGColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.0, 1.0]
        
        avatarBG.backgroundColor = UIColor.clearColor()
        gl.frame = CGRectMake(0, 0, 50, 50)
        avatarBG.layer.insertSublayer(gl, atIndex: 0)
    }
    
}


