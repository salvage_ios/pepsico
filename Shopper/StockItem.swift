//
//  StockItem.swift
//  Shopper
//
//  Created by VividMacmini7 on 27/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FirebaseDatabase

struct StockItem {
    let key:String
    var name:String
    var price:String
    var Qty:String
    var tax:String
    var discount:String
    let ref: FIRDatabaseReference?
    
    init(name:String, Qty:String, price:String, tax:String, discount:String, key:String = "") {
        self.key = key
        self.name = name
        self.Qty = Qty
        self.price = price
        self.tax = tax
        self.discount = discount
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        name = snapshotValue["item_name"] as! String
        price = snapshotValue["item_price"] as! String
        Qty = snapshotValue["item_qty"] as! String
        tax = snapshotValue["item_tax"] as! String
        discount = snapshotValue["item_discount"] as! String
        ref = snapshot.ref
    }
    
    func toAnyObject() -> AnyObject {
        return [
            "item_name": name,
            "item_price": price,
            "item_qty": Qty ,
            "item_tax" : tax ,
            "item_discount" : discount
        ]
    }
}
