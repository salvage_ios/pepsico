//
//  PackOrder.swift
//  Shopper
//
//  Created by VividMacmini7 on 04/04/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class PackOrder: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var orderListTable:UITableView!
    @IBOutlet var area: UILabel!
    
    @IBOutlet var orderId: UILabel!
    @IBOutlet var orderTakenBy: UILabel!
    @IBOutlet var orderDate: UILabel!
    @IBOutlet var barButton:UIBarButtonItem!
    
    var orderDetail:OrderDetailModel!
    var orderList = [OrderModel]()
    var isProductPacked = [Bool]()
    var readyToBillOrder = false
    var orderListRef = FIRDatabase.database().referenceWithPath("order_list")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Pack Order"
        orderListTable.tableFooterView = UIView()
        
        orderId.text = orderDetail.orderId
        orderTakenBy.text = orderDetail.orderTakenBy
        orderDate.text = orderDetail.orderDate
        area.text = orderDetail.shopArea

        barButton.enabled = false
    }
    
    override func viewWillAppear(animated: Bool) {
        getOrderList()
    }
    
    func getOrderList() {
        isProductPacked = [Bool]()
        orderList = [OrderModel]()
        
        orderListRef = orderListRef.child(orderDetail.orderId)
        orderListRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if !(snapshot.value is NSNull) {
                for child in snapshot.children {
                    let order = OrderModel(snapshot: child as! FIRDataSnapshot)
                    self.orderList.append(order)
                    self.isProductPacked.append(false)
                }
                dispatch_async(dispatch_get_main_queue(), {
                    self.orderListTable.reloadData()
                })
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: UITableView DataSouce:
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PackOrderCell", forIndexPath: indexPath)
        
        cell.textLabel?.attributedText = NSAttributedString()
        cell.textLabel?.textColor = UIColor.blackColor()
        cell.textLabel?.text = orderList[indexPath.row].productName
        cell.detailTextLabel?.text = orderList[indexPath.row].productQty
        cell.accessoryType = .None
        
        if isProductPacked[indexPath.row] {
            cell.textLabel?.textColor = UIColor.grayColor()
            cell.accessoryType = .Checkmark
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: orderList[indexPath.row].productName)
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))
            cell.textLabel?.attributedText = attributeString
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if isProductPacked[indexPath.row] {
            self.isProductPacked.removeAtIndex(indexPath.row)
            self.isProductPacked.insert(false, atIndex: indexPath.row)
        }else{
            self.isProductPacked.removeAtIndex(indexPath.row)
            self.isProductPacked.insert(true, atIndex: indexPath.row)
        }
        if !isProductPacked.contains(false) {
            barButton.enabled = true
        }else{
            barButton.enabled = false
        }
        self.orderListTable.reloadData()
    }
    
    //MARK: IBAction:
    @IBAction func generateBill(sender:UIBarButtonItem) {
        
        let user = HelperClass.userDetails()
        
        orderDetail.ref?.updateChildValues([
            "order_packedBy": (user?.uName)!,
            "order_status": 2,
            ])
        
        
        let previewAlert = UIAlertController(title: nil, message: "Bill Generated, \n Order Status Has Been Updated", preferredStyle: .Alert)
        previewAlert.addAction(UIAlertAction(title: "Preview", style: .Default, handler: { (alert) in
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PreviewViewController") as! PreviewViewController
            vc.orderDetail = self.orderDetail
            vc.orderList = self.orderList
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        previewAlert.addAction(UIAlertAction(title: "Okay", style: .Cancel, handler: { (alert) in
            self.navigationController?.popViewControllerAnimated(true)
        }))
        self.navigationController?.presentViewController(previewAlert, animated: true, completion: nil)
    }
    
}

