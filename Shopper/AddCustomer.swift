//
//  AddCustomer.swift
//  Shopper
//
//  Created by VividMacmini7 on 27/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class AddCustomer: UITableViewController,UITextFieldDelegate {

    @IBOutlet var shopName:UITextField!
    @IBOutlet var shopAddress:UITextField!
    @IBOutlet var customerName:UITextField!
    @IBOutlet var customerPoneNumber:UITextField!
    @IBOutlet var shopArea:UITextField!
    @IBOutlet var city:UITextField!
    @IBOutlet var zipCode:UITextField!
    
    var customerDetail:CustomerModel!
    
    let ref = FIRDatabase.database().referenceWithPath("customer_list")
    var latlngCompletion:((latlng:String) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if customerDetail != nil {
            shopName.text = customerDetail.shopName
            shopAddress.text = customerDetail.shopAddress
            customerName.text = customerDetail.customerName
            customerPoneNumber.text = customerDetail.customerPhone
            shopArea.text = customerDetail.shopArea
            city.text = customerDetail.shopCityState
            zipCode.text = customerDetail.shopZipCode
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(areaSelected(_:)), name: "keyAreaSelected", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: UITextFieldDelegate:
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField.tag == 10 {
            let vc:AreaList = self.storyboard?.instantiateViewControllerWithIdentifier("AreaList") as! AreaList
            let nav = UINavigationController(rootViewController: vc)
            self.navigationController?.presentViewController(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: UITextFieldDelegate:
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 15 || textField.tag == 32  {
            let numbersOnly = NSCharacterSet(charactersInString: "1234567890")
            let characterSetFromTextField = NSCharacterSet(charactersInString: string)
            
            let Validate:Bool = numbersOnly .isSupersetOfSet(characterSetFromTextField)
            if !Validate {
                return false;
            }
            if range.length + range.location > textField.text?.characters.count {
                return false
            }
            let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
            
            let maxLimit = textField.tag == 15 ? 11 : 6;
            return newLength <= maxLimit
        }
        return true
    }
    
    //MARK: UITableviewDataSource:
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    //MARK: Support
    func latlngFromAddress(address:String, handler: ((value:String) -> Void)?) {
        var directionsURLString = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address)&key=\(googleMapsApiKey)"
        directionsURLString = directionsURLString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let directionsURL = NSURL(string: directionsURLString)
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let directionsData = NSData(contentsOfURL: directionsURL!)
            do{
                var dictionary: Dictionary<NSObject, AnyObject> = try NSJSONSerialization.JSONObjectWithData(directionsData!, options: NSJSONReadingOptions.MutableContainers) as! Dictionary<NSObject, AnyObject>
                print(dictionary)
                let result = dictionary["results"] as! NSArray
                if result.count == 0 {
                    return handler!(value: "")
                }
                let geo = result[0].valueForKey("geometry") as! NSDictionary
                let location = geo.valueForKey("location") as! NSDictionary
                print("\(location.valueForKey("lat")!),\(location.valueForKey("lng")!)")
                dispatch_async(dispatch_get_main_queue(), {
                    return handler!(value: "\(location.valueForKey("lat")!),\(location.valueForKey("lng")!)")
                })
            }
            catch {
                dispatch_async(dispatch_get_main_queue(), {
                    return handler!(value: "")
                })
            }
        })
    }
    
    
    
    //MARK: IBAction

    func areaSelected(sender:NSNotification) {
        let userInfo = sender.userInfo
        shopArea.text = userInfo!["area"] as? String
    }
    
    @IBAction func cancel(sender: UIButton) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
   
    @IBAction func save(sender: UIButton) {
        if validateAllFields() {
            let address = "\(shopAddress.text!),\(city.text!),\(zipCode.text!)"
           self.latlngFromAddress(address, handler: { (latlng) in
            print(latlng)
            if self.customerDetail != nil {
                self.customerDetail.ref?.updateChildValues([
                    "customer_name": self.customerName.text!,
                    "customer_phone": self.customerPoneNumber.text!,
                    "shop_name": self.shopName.text!,
                    "shop_address": self.shopAddress.text!,
                    "shop_latlng" : latlng,
                    "shop_area": self.shopArea.text! ,
                    "shop_cityState": self.city.text! ,
                    "shop_zip": self.zipCode.text!
                    ])
                
                let alert = UIAlertController(title: nil, message: "\(self.customerName.text!) Profile Updated Successfully", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: { (action) in
                    self.navigationController!.popToRootViewControllerAnimated(true)
                }))
                self.navigationController?.presentViewController(alert, animated: true, completion: nil)
            }else{
                let name = self.shopName.text
                
                let customerDetails = CustomerModel(name: self.customerName.text!, phone: self.customerPoneNumber.text!, shopName: self.shopName.text!, address: self.shopAddress.text!, shopArea: self.shopArea.text!, cityState: self.city.text!, Zip: self.zipCode.text!, orderList: [], latlng: self.customerName.text!)
                
                let itemRef = self.ref.child(name!.lowercaseString)
                itemRef.setValue(customerDetails.toAnyObject())
                
                let alert = UIAlertController(title: nil, message: "Customer Added Successfully", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: { (action) in
                    self.navigationController!.popViewControllerAnimated(true)
                }))
                self.navigationController?.presentViewController(alert, animated: true, completion: nil)
            }
           })
            
            
        }else{
            HelperClass.showSimpleAlertWith("Enter All Fields", inVC: self)
        }
                
    }
    
    func validateAllFields() -> Bool {
        if shopAddress.text == nil || shopName.text == nil || shopArea.text == nil || customerName.text == nil || customerPoneNumber.text == nil  || city.text == nil || zipCode.text == nil{
            return false
        }
        return true
    }
    

}
