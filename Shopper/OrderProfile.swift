//
//  OrderProfile.swift
//  Shopper
//
//  Created by VividMacmini7 on 03/04/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class OrderProfile: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    //MARK: IBOutlet:
    @IBOutlet var orderTakenby: UILabel!
    @IBOutlet var orderPackedby: UILabel!
    @IBOutlet var orderDeliveredby: UILabel!
    
    @IBOutlet var orderId: UILabel!
    @IBOutlet var orderDate: UILabel!
    @IBOutlet var shopArea: UILabel!
    
    @IBOutlet var orderTaken: UILabel!
    @IBOutlet var orderpacked: UILabel!
    @IBOutlet var orderDelivered: UILabel!
    @IBOutlet var connectorOne:UIView!
    @IBOutlet var connectorTwo:UIView!
    
    @IBOutlet var actualAmount: UILabel!
    @IBOutlet var taxAdded: UILabel!
    @IBOutlet var discount: UILabel!
    
    @IBOutlet var totalAmount: UILabel!
    @IBOutlet var totalAmountTwo: UILabel!
    @IBOutlet var paidAmount: UILabel!
    @IBOutlet var pendingAmount: UILabel!
    
    @IBOutlet var paymentMethod: UILabel!
    @IBOutlet var chequeNumber: UILabel!
    
    @IBOutlet var orderDetailTable: UITableView!
    
    @IBOutlet var containerOne: UIView!
    @IBOutlet var containerTwo: UIView!
    
    @IBOutlet var makePayment: UIButton!
    @IBOutlet var delivered: UIButton!
    
   
    //MARK: Variable:
    var orderIdStr = ""
    var orderDetail:OrderDetailModel!
    var orderList = [OrderModel]()
    var orderListRef = FIRDatabase.database().referenceWithPath("order_list")
    var orderDetailRef = FIRDatabase.database().referenceWithPath("order_Details")
    var paymentMade = false
    var dateFormate = Bool()
    var chequeDateTextField: UITextField!
    var userDetails:UserModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userDetails = HelperClass.userDetails()
        
        var button = UIButton()
        button.frame = CGRectMake(0, 0, 25, 25)
        button.setImage(UIImage(named: "Call"), forState: .Normal)
        button.addTarget(self, action: #selector(makeCall(_:)), forControlEvents: .TouchUpInside)
        button.tintColor = UIColor.appTheme()
        
        let barbuttonCall = UIBarButtonItem()
        barbuttonCall.customView = button
        
        button = UIButton()
        button.frame = CGRectMake(0, 0, 25, 25)
        button.setImage(UIImage(named: "Edit"), forState: .Normal)
        button.addTarget(self, action: #selector(editOrder(_:)), forControlEvents: .TouchUpInside)
        button.tintColor = UIColor.appTheme()
        
        let barbuttonEdit = UIBarButtonItem()
        barbuttonEdit.customView = button
        
        button = UIButton()
        button.frame = CGRectMake(0, 0, 25, 25)
        button.setImage(UIImage(named: "printer"), forState: .Normal)
        button.addTarget(self, action: #selector(exportPdf(_:)), forControlEvents: .TouchUpInside)
        button.tintColor = UIColor.appTheme()
        
        let barbuttonPrint = UIBarButtonItem()
        barbuttonPrint.customView = button
        
        let space = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
        space.width = 15
        
        if userDetails.uType == .UserTypAdmin {
            self.navigationItem.rightBarButtonItems = [barbuttonEdit,space,barbuttonCall,space,barbuttonPrint]
        }else{
            self.navigationItem.rightBarButtonItems = [barbuttonCall,space,barbuttonPrint]
        }
        orderDetailTable.estimatedRowHeight = 44
        orderDetailTable.tableFooterView = UIView()
        self.view.userInteractionEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
         getOrderDetails()
    }
    
    //MARK: UITableViewDataSource:
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderList.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("OrderProfileTableCell", forIndexPath: indexPath) as! PreviewTableCell
        if indexPath.row == 0 {
            cell.sNo.text = "#"
            cell.sNo.font = UIFont.AppFontBold()
            
            cell.productName.text = "Product Name"
            cell.productName.font = UIFont.AppFontBold()
            
            cell.productQty.text = "Qty"
            cell.productQty.font = UIFont.AppFontBold()
            
            cell.productPrice.text = "Price"
            cell.productPrice.font = UIFont.AppFontBold()
            
            cell.productTotalPrice.text = "Total"
            cell.productTotalPrice.font = UIFont.AppFontBold()
            
            cell.productTax.text = "Tax"
            cell.productTax.font = UIFont.AppFontBold()
            
            cell.productDisc.text = "Disc"
            cell.productDisc.font = UIFont.AppFontBold()
            
        }else{
            
            cell.backgroundColor = even(indexPath.row) ? UIColor.selectedStockBG() : UIColor.whiteColor()
            
            cell.sNo.text = String(indexPath.row)
            cell.sNo.font = UIFont.AppFontRegular()
            
            cell.productName.text = orderList[indexPath.row - 1].productName
            cell.productName.font = UIFont.AppFontRegular()
            
            cell.productQty.text = orderList[indexPath.row - 1].productQty
            cell.productQty.font = UIFont.AppFontRegular()
            
            cell.productPrice.text = orderList[indexPath.row - 1].productPrice
            cell.productPrice.font = UIFont.AppFontRegular()
            
            cell.productTax.text = "\(orderList[indexPath.row - 1].productTax)%"
            cell.productTax.font = UIFont.AppFontRegular()
            
            cell.productDisc.text = "\(orderList[indexPath.row - 1].productDisc)%"
            cell.productDisc.font = UIFont.AppFontRegular()
            
            cell.productTotalPrice.text = orderList[indexPath.row - 1].productTotalPrice
            cell.productTotalPrice.font = UIFont.AppFontRegular()
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    
   
    //MARK:@IBAction
    
    @IBAction func editOrder(sender: UIBarButtonItem) {
        if userDetails.uType != .UserTypAdmin {
            HelperClass.showSimpleAlertWith("You don't have permission to edit order \n Contact owner for more info.", inVC: self)
            return
        }
        if orderDetail.orderStatus == .OrderPacked {
            HelperClass.showSimpleAlertWith("Unable to edit order \nIt has been packed and billed.", inVC: self)
        }else if orderDetail.orderStatus == .OrderDelivered {
            HelperClass.showSimpleAlertWith("Unable to edit order \nOrder has been already delivered to customer.", inVC: self)
        }else{
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EditOrder") as! EditOrder
            vc.oldOrderListModel = orderList
            vc.oldOrderDetail = orderDetail
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    @IBAction func makeCall(sender: UIBarButtonItem) {
        let url:NSURL = NSURL(string: "tel:\(orderDetail.shopPhone)")!
        UIApplication.sharedApplication().openURL(url)
    }
    
    @IBAction func exportPdf(sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PreviewViewController") as! PreviewViewController
        vc.orderDetail = self.orderDetail
        vc.orderList = self.orderList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func makePayment(sender: UIButton) {
        if orderDetail.orderStatus != .OrderTaken {
            var paymentMessage = "Name: \(self.orderDetail.shopOwner) \n Order Qty: \(self.orderDetail.orderQty) Products \n Total: \(self.orderDetail.orderPrice)"
            var completionMessage = "Payment Done & Order has been marked as delivered"
            var paiedAmount:Float = 0
            if sender.titleLabel?.text == "Pending Payment" {
                let pending = Float(self.orderDetail.pendingAmount)
                let actual = Float(self.orderDetail.orderPrice)
                
                let old = actual! - pending!
                print(self.orderDetail.paymentMethod)
                let lastPaymentMethod = self.orderDetail.paymentMethod == .PayWithCash ? "Cash" : "Cheque"
                paymentMessage = "Name: \(self.orderDetail.shopOwner) \n Order Qty: \(self.orderDetail.orderQty) Products \n Total: \(self.orderDetail.orderPrice)\n Old Pay: \(old) \n Pending: \(self.orderDetail.pendingAmount) \n Last Payment Method :\(lastPaymentMethod)"
                completionMessage = "Payment Done "
                
                paiedAmount = old
            }
            
            // *** ActionSheet *** //
            let actionsheet = UIAlertController(title: "Payment Type", message: nil, preferredStyle: .ActionSheet)
            actionsheet.addAction(UIAlertAction(title: "Cash", style: .Default, handler: { (action) in
                dispatch_async(dispatch_get_main_queue(), {
                    let alert = UIAlertController(title: "Cash Payment", message: paymentMessage, preferredStyle: .Alert)
                    alert.addTextFieldWithConfigurationHandler({ (textfield) in
                        textfield.placeholder = "Amount"
                        textfield.keyboardType = .DecimalPad
                    })
                    
                    alert.addAction(UIAlertAction(title: "Pay", style: .Default, handler: { (action) in
                        dispatch_async(dispatch_get_main_queue(), {
                            self.paymentByCash(alert, alreadyPaidAmount: paiedAmount, completionMessage: completionMessage)
                            })
                        
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
                    self.navigationController?.presentViewController(alert, animated: true, completion: nil)
                })
            }))
            
            actionsheet.addAction(UIAlertAction(title: "Cheque", style: .Default, handler: { (action) in
                dispatch_async(dispatch_get_main_queue(), {
                    let alert = UIAlertController(title: "Cheque Payment", message: paymentMessage , preferredStyle: .Alert)
                    alert.addTextFieldWithConfigurationHandler({ (textfield) in
                        textfield.placeholder = "Bank Name"
                        textfield.autocapitalizationType = .Words
                    })
                    alert.addTextFieldWithConfigurationHandler({ (textfield) in
                        textfield.placeholder = "Cheque Number"
                        textfield.keyboardType = .DecimalPad
                    })
                    alert.addTextFieldWithConfigurationHandler({ (textfield) in
                        textfield.placeholder = "Cheque Amount"
                        textfield.keyboardType = .NumberPad
                    })
                    alert.addTextFieldWithConfigurationHandler({ (textfield) in
                        self.chequeDateTextField = textfield
                        textfield.tag = 20
                        textfield.placeholder = "Check Date"
                        textfield.keyboardType = .DecimalPad
                        textfield.delegate = self
                    })
                    
                    alert.addAction(UIAlertAction(title: "Pay", style: .Default, handler: { (action) in
                        self.payByCheque(alert, alreadyPaidAmount: paiedAmount, completionMessage: completionMessage)
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
                    self.navigationController?.presentViewController(alert, animated: true, completion: nil)
                })
            }))
            actionsheet.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
            self.navigationController?.presentViewController(actionsheet, animated: true, completion: nil)
            

        }else{
            HelperClass.showSimpleAlertWith("Order has not been packed yet, \n It can be delivered only after packing", inVC: self)
        }
        
    }
    
   
    @IBAction func orderDelivered(sender: UIButton) {
        if orderDetail.orderStatus != .OrderTaken {
            if sender.titleLabel?.text == "Already Delivered" {
                HelperClass.showSimpleAlertWith("Order has been already delivered", inVC: self)
            }else{
                if self.paymentMade {
                    let alert = UIAlertController(title: "Order Delivered ?", message: "This action can't be undo", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Delivered", style: .Default, handler: { (action) in
                        self.orderDetail.orderStatus = OrderStatus.OrderDelivered
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
                    self.navigationController?.presentViewController(alert, animated: true, completion: nil)
                }else{
                    let alert = UIAlertController(title: "Payment not made !!! ", message: "Consider this order price as pending amount ?", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "YES", style: .Default, handler: { (action) in
                        self.orderDetail.ref?.updateChildValues([
                            "paid_amount" : "" ,
                            "pending_amount" : self.orderDetail.orderPrice ,
                            "order_deliveredBy" : self.userDetails.uName,
                            "order_status" : 3
                            ])
                        self.navigationController?.popViewControllerAnimated(true)
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
                    self.navigationController?.presentViewController(alert, animated: true, completion: nil)
                }
                
            }
        }else{
            HelperClass.showSimpleAlertWith("Order has not been packed yet, \n It can be delivered only after packing", inVC: self)
        }
        
        
    }
    
    @IBAction func datePicker(sender: UIDatePicker) {
        let dateFormate = NSDateFormatter()
        dateFormate.dateFormat = "dd-MM-yyyy"
        print(dateFormate.stringFromDate(sender.date))
        chequeDateTextField.text = dateFormate.stringFromDate(sender.date)
    }
    
    //MARK: UITextFieldDelegate:
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField.tag == 20 {
            let date = UIDatePicker()
            date.minimumDate = NSDate()
            date.setDate(NSDate(), animated: true)
            date.datePickerMode = .Date
            date.addTarget(self, action: #selector(datePicker(_:)), forControlEvents: .ValueChanged)
            textField.inputView = date
        }
    }
    
//    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        
//        //1. To make sure that this is applicable to only particular textfield add tag.
//        if textField.tag == 20 {
//            //2. this one helps to make sure that user enters only numeric characters and '-' in fields
//            let numbersOnly = NSCharacterSet(charactersInString: "0123456789-")
//            let characterSetFromTextField = NSCharacterSet(charactersInString: string)
//            
//            let Validate:Bool = numbersOnly .isSupersetOfSet(characterSetFromTextField)
//            if !Validate {
//                return false;
//            }
//            if range.length + range.location > textField.text?.characters.count {
//                return false
//            }
//            let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
//            if newLength == 3 || newLength == 6 {
//                let  char = string.cStringUsingEncoding(NSUTF8StringEncoding)!
//                let isBackSpace = strcmp(char, "\\b")
//                
//                if (isBackSpace == -92) {
//                    dateFormate = false;
//                }else{
//                    dateFormate = true;
//                }
//                
//                if dateFormate {
//                    let textContent:String!
//                    textContent = textField.text
//                    //3.Here we add '-' on overself.
//                    let textWithHifen:NSString = "\(textContent)-"
//                    textField.text = textWithHifen as String
//                    dateFormate = false
//                }
//            }
//            //4. this one helps to make sure only 8 character is added in textfield .(ie: dd-mm-yy)
//            return newLength <= 10;
//            
//        }
//        return true
//    }
    
    
    
    //MARK: Support:
    func even(number: Int) -> Bool {
        // Return true if number is evenly divisible by 2.
        return number % 2 == 0
    }
    
    func getOrderDetails() {
        orderDetailRef = orderDetailRef.child(orderIdStr)
        orderDetailRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if !(snapshot.value is NSNull) {
                let stock = OrderDetailModel(snapshot: snapshot)
                self.orderDetail = stock
                dispatch_async(dispatch_get_main_queue(), {
                    self.getOrderList()
                })
                
            }
        })
    }
    
    func getOrderList() {
        orderListRef = orderListRef.child(orderIdStr)
        orderListRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if !(snapshot.value is NSNull) {
                for child in snapshot.children {
                    let order = OrderModel(snapshot: child as! FIRDataSnapshot)
                    self.orderList.append(order)
                }
                dispatch_async(dispatch_get_main_queue(), {
                    self.loadUI()
                    self.orderDetailTable.reloadData()
                })
                
            }
        })
    }
    
    func loadUI () {
        self.title = orderDetail.shopName
        orderTakenby.text = orderDetail.orderTakenBy
        orderPackedby.text = orderDetail.orderPackedBy.characters.count == 0 ? "NA" : orderDetail.orderPackedBy
        orderDeliveredby.text = orderDetail.orderDeliveredBy.characters.count == 0 ? "NA" : orderDetail.orderDeliveredBy
        
        orderId.text = orderDetail.orderId
        orderDate.text = orderDetail.orderDate
        shopArea.text = orderDetail.shopArea
        
        let totalPrice = Float(orderDetail.orderPrice)
        let tax = Float(orderDetail.orderTax)
        let disc = Float(orderDetail.orderDiscount)
        
        actualAmount.text = "\((totalPrice! - tax!) + disc!)"
        taxAdded.text = "+\(orderDetail.orderTax)"
        discount.text = "-\(orderDetail.orderDiscount)"
        
        totalAmount.text = orderDetail.orderPrice
        totalAmountTwo.text = orderDetail.orderPrice
        paidAmount.text = orderDetail.paidAmount == "" ? "NA" : orderDetail.paidAmount
        pendingAmount.text = orderDetail.pendingAmount == "" ? orderDetail.orderPrice : orderDetail.pendingAmount
        paymentMethod.text = orderDetail.paidAmount == "" ? "NA" : orderDetail.paymentMethod == .PayWithCash ? "Cash" : "Cheque"
        chequeNumber.text = orderDetail.paymentMethod == .PayWithCash ? "NA" : orderDetail.chequeNumber != "" ? orderDetail.chequeNumber : "NA"
        
        let status = orderDetail.orderStatus
        if status == .OrderTaken {
            connectorOne.backgroundColor = UIColor.lightGrayColor()
            connectorTwo.backgroundColor = UIColor.lightGrayColor()
            
            orderTaken.backgroundColor = UIColor.appThemeYellow()
            orderpacked.backgroundColor = UIColor.lightGrayColor()
            orderDelivered.backgroundColor = UIColor.lightGrayColor()
            
        }else if status == .OrderPacked {
            connectorOne.backgroundColor = UIColor.appThemeGreen()
            connectorTwo.backgroundColor = UIColor.lightGrayColor()
            
            orderTaken.backgroundColor = UIColor.appThemeGreen()
            orderpacked.backgroundColor = UIColor.appThemeGreen()
            orderDelivered.backgroundColor = UIColor.lightGrayColor()
            
        }else if status == .OrderDelivered {
            connectorOne.backgroundColor = UIColor.appTheme()
            connectorTwo.backgroundColor = UIColor.appTheme()
            
            orderTaken.backgroundColor = UIColor.appTheme()
            orderpacked.backgroundColor = UIColor.appTheme()
            orderDelivered.backgroundColor = UIColor.appTheme()
        }
        
        
        if HelperClass.userDetails()?.uType == .UserTypeDriver {
            if orderDetail.orderStatus == .OrderDelivered && orderDetail.pendingAmount == "0"{
                containerOne.hidden = false
                containerTwo.hidden = true
            }else if orderDetail.orderStatus == .OrderDelivered && orderDetail.pendingAmount != "0" {
                containerOne.hidden = true
                containerTwo.hidden = false
                
                makePayment.setTitle("Pending Payment", forState: .Normal)
                delivered.setTitle("Already Delivered", forState: .Normal)
                
            }else{
                containerOne.hidden = true
                containerTwo.hidden = false
            }
            
        }else{
            containerOne.hidden = false
            containerTwo.hidden = true
        }
        
        self.view.userInteractionEnabled = true
    }
    
    func paymentByCash(alert:UIAlertController, alreadyPaidAmount:Float , completionMessage:String) {
        
        var paiedAmount = alreadyPaidAmount
        if alert.textFields![0].text?.characters.count == 0 {
            dispatch_async(dispatch_get_main_queue(), {
                HelperClass.showSimpleAlertWith("Enter Bill Amount Before Making Payment", inVC: self)
            })
            return
        }
        
        paiedAmount = paiedAmount + Float(alert.textFields![0].text!)!
        let actualAmount = Float(self.orderDetail.orderPrice)
        if paiedAmount > actualAmount {
            HelperClass.showSimpleAlertWith("Amount entered is greater than actual amount, Payment can't be processed. \n Please try again.", inVC: self)
            
        } else if paiedAmount <= actualAmount {
            
                let balance = actualAmount! - paiedAmount
                HelperClass.showSimpleAlertWith("\(completionMessage)  \n Total Paid : \(paiedAmount) \n Amount Balance :\(balance) ₹", inVC: self)
                self.paymentMade = true
                self.orderDetail.ref?.updateChildValues([
                    "paid_amount" : "\(paiedAmount)" ,
                    "pending_amount" : balance == 0 ? "0" : "\(balance)" ,
                    "payment_method" : 1 ,
                    "order_deliveredBy" : (HelperClass.userDetails()?.uName)! ,
                    "order_status" : 3
                    ])
                
                
                    let al = UIAlertController(title: "Payment Details", message: "\(completionMessage)  \n Total Paid : \(paiedAmount) \n Amount Balance :\(balance) ₹", preferredStyle: .Alert)
                    al.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: { (action) in
                        print("cancel")
                        dispatch_async(dispatch_get_main_queue(), {
                            self.navigationController?.popViewControllerAnimated(true)
                            })
                        
                    }))
                    self.navigationController?.presentViewController(al, animated: true, completion: nil)
                }
                
        }
    
    
    
    func payByCheque (alert:UIAlertController, alreadyPaidAmount:Float , completionMessage:String) {
        if alert.textFields![0].text!.characters.count != 0 && alert.textFields![1].text!.characters.count != 0 && alert.textFields![2].text!.characters.count != 0 && alert.textFields![3].text!.characters.count != 0 {
            var paiedAmount = alreadyPaidAmount
            
            paiedAmount = paiedAmount + Float(alert.textFields![2].text!)!
            
            let actualAmount = Float(self.orderDetail.orderPrice)
            if paiedAmount > actualAmount {
                HelperClass.showSimpleAlertWith("Amount entered is greater than actual amount, Payment can't be processed. \n Please try again.", inVC: self)
                
            }else if paiedAmount <= actualAmount {
                dispatch_async(dispatch_get_main_queue(), {
                    let balance = actualAmount! - paiedAmount
                    HelperClass.showSimpleAlertWith("\(completionMessage) \n Total Paid : \(paiedAmount) ₹\n Amount Balance : \(balance) ₹", inVC: self)
                    self.paymentMade = true
                    self.orderDetail.ref?.updateChildValues([
                        "paid_amount" : "\(paiedAmount)" ,
                        "pending_amount" : balance == 0 ? "0" : "\(balance)" ,
                        "payment_method" : 2 ,
                        "cheque_number" : alert.textFields![0].text! ,
                        "order_deliveredBy" : (HelperClass.userDetails()?.uName)!,
                        "order_status" : 3
                        ])
                    
                    self.navigationController?.popViewControllerAnimated(true)
                    
                })
            }
        }else{
            dispatch_async(dispatch_get_main_queue(), {
                HelperClass.showSimpleAlertWith("Enter Cheque Number And Bill Amount Before Making Payment", inVC: self)
            })
            return
        }
    }
}
