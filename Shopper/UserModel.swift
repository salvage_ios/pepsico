//
//  UserModel.swift
//  Shopper
//
//  Created by VividMacmini7 on 30/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct UserModel {
    let key:String
    
    let uId:String
    let uName:String
    let uPhone:String
    let uEmail:String
    let uType:UserType
    
    let ref: FIRDatabaseReference?
    
    init(id:String,name:String, phone:String, email:String, type:UserType,key:String = "") {
        uId = id
        uName = name
        uPhone = phone
        uEmail = email
        uType = type
        
        self.key = key
        self.ref = nil
    }
    
    init (snapShot:AnyObject) {
        key = ""
        uId = snapShot["u_id"] as! String
        uName = snapShot["u_name"] as! String
        uPhone = snapShot["u_phone"] as! String
        uEmail = snapShot["u_email"] as! String
        let type = snapShot["u_type"] as! UserType.RawValue
        switch type {
        case 0:
            uType = .UserTypAdmin
            break
        case 1:
            uType = .UserTypeSalesman
            break
        case 2:
            uType = .UserTypeDriver
            break
        default:
            uType = .UserTypAdmin
            break
        }
        self.ref = nil
    }
    
    init (snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapShot = snapshot.value as! [String: AnyObject]
        
        uId = snapShot["u_id"] as! String
        uName = snapShot["u_name"] as! String
        uPhone = snapShot["u_phone"] as! String
        uEmail = snapShot["u_email"] as! String
        
        let type = snapShot["u_type"] as! UserType.RawValue
        switch type {
        case 0:
            uType = .UserTypAdmin
            break
        case 1:
            uType = .UserTypeSalesman
            break
        case 2:
            uType = .UserTypeDriver
            break
        default:
            uType = .UserTypAdmin
            break
        }
        
        self.ref = snapshot.ref
    }
    
    func toAnyObject() -> AnyObject {
        return [
            "u_id": uId,
            "u_name": uName,
            "u_phone": uPhone,
            "u_email": uEmail,
            "u_type": uType.rawValue,
        ]
    }
}

