//
//  DriverMapView.swift
//  Shopper
//
//  Created by VividMacmini7 on 10/04/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import GoogleMaps
import AVFoundation

class DriverMapView: UIViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate,GMSMapViewDelegate {
    
    @IBOutlet var visualEffectView: UIVisualEffectView!
    @IBOutlet var orderCount: UILabel!
    @IBOutlet var nightMode: UIButton!
    @IBOutlet var currenLoc: UIButton!
    @IBOutlet var traffic: UIButton!
    
    @IBOutlet var dliveryListTable: UITableView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var deliveryCount: UILabel!
    
    
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    let speechSynthesizer = AVSpeechSynthesizer()
    
    var deliveryList = [OrderDetailModel]()
    var orderToBeDelivered:OrderDetailModel!
    
    
    var selectedRoute: Dictionary<NSObject, AnyObject>!
    var overviewPolyline: Dictionary<NSObject, AnyObject>!
    
    let locationManager = CLLocationManager()
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var locValue:CLLocationCoordinate2D!
    
    var originMarker = GMSMarker()
    var destinationMarker = GMSMarker()
    var routePolyline = GMSPolyline()
    
    var trackDriverLocation = false
    var isNightMode = false
    var audioAssistance = false
    
    var oldDirectionInstruction = ""
    
    let frameHeight = UIScreen.mainScreen().bounds.height
    
    
    lazy var minY:CGFloat = {
        return self.frameHeight == 568 ? self.getPercentage(92) : self.getPercentage(94)
    }()
    
    lazy var midY:CGFloat = {
        return self.frameHeight == 568 ? self.getPercentage(58) : self.getPercentage(60)
    }()
    
    lazy var minY1:CGFloat = {
        return self.frameHeight == 568 ? self.getPercentage(8) : self.getPercentage(6)
    }()
    
    lazy var midY1:CGFloat = {
        return self.frameHeight == 568 ? self.getPercentage(42) : self.getPercentage(40)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mapView.mapType = GoogleMaps.kGMSTypeNormal
        nightMode.tag = 10
        traffic.tag = 10
        mapView.delegate = self
        routePolyline.strokeWidth = 3
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        for order in deliveryList {
            let latLng = order.shoplatLng.componentsSeparatedByString(",")
            let coordinates = CLLocationCoordinate2D(latitude: Double(latLng[0])!, longitude: Double(latLng[1])!)
            let marker = GMSMarker(position: coordinates)
            marker.map = mapView
            marker.iconView = UIImageView(image: UIImage(named: "shop"))
            marker.title = order.shopName
        }
//        self.locationManager.requestAlwaysAuthorization()
//        self.locationManager.requestWhenInUseAuthorization()
        
        orderCount.text = "My Deliveries (\(deliveryList.count))"
        deliveryCount.text = "You have \(deliveryList.count) deliveries"
        if deliveryList.count == 0 {
            let backgroundImage = UIImage(named: "TableBG1")
            let imageView = UIImageView(image: backgroundImage)
            self.dliveryListTable.backgroundView = imageView
        }
        
        visualEffectView.layer.cornerRadius = 10.0
        visualEffectView.layer.masksToBounds = true
        visualEffectView.frame = CGRectMake(0, minY, self.view.frame.size.width, self.view.frame.size.height)
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        visualEffectView.addGestureRecognizer(gesture)
        
        self.view.addSubview(visualEffectView)
        self.view.bringSubviewToFront(visualEffectView)
        self.view.bringSubviewToFront(deliveryCount)
        self.view.bringSubviewToFront(visualEffectView)
        self.view.bringSubviewToFront(currenLoc)
        self.view.bringSubviewToFront(nightMode)
        self.view.bringSubviewToFront(traffic)
        
        dliveryListTable.tableFooterView = UIView()
        dliveryListTable.reloadData()
        deliveryCount.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
    
    
    //MARK: UIPanGestureRecognizer:
    func panGesture(gestureRecognizer:UIPanGestureRecognizer) {
        switch gestureRecognizer.state {
        case .Began, .Changed:
            let translation = gestureRecognizer.translationInView(self.view)
            gestureRecognizer.view!.center = CGPointMake(gestureRecognizer.view!.center.x, gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPointMake(0,0), inView: self.view)
            break
            
        case .Ended:
            var currentY = (gestureRecognizer.view?.frame.origin.y)! - (gestureRecognizer.view?.frame.size.height)!
            if currentY < 0 {
                currentY = currentY * -1
            }
            if currentY >= midY1 / 2 && gestureRecognizer.view?.frame.origin.y <= midY1 {
                currentY = midY
            }else if currentY < midY1/2 {
                currentY = minY
            }else if currentY > midY1 {
                currentY = midY
            }else{
                currentY = midY
            }
            
            visualEffectView.frame = CGRectMake(0, currentY, self.view.frame.size.width, self.view.frame.size.height)
            
            break
            
        default:
            break
        }
        
    }
    
    func getPercentage(percent:NSInteger) -> CGFloat {
        let frameHeight = self.view.frame.size.height
        let returnPercentage = (frameHeight / 100) * CGFloat(percent)
        let roundedSliderValue =  Double(round(10 * returnPercentage)/10)
        return CGFloat(roundedSliderValue)
    }
    
    //MARK: IBAction:
    @IBAction func showCurrentLocation(sender:UIButton) {
        let camera = GMSCameraPosition.cameraWithTarget(locValue, zoom: 20, bearing: 270, viewingAngle: 45)
        mapView.camera = camera
    }
    

    @IBAction func showTraffic(sender:UIButton) {
        if sender.tag == 10 {
            sender.tag = 20
            mapView.trafficEnabled = true
        }else{
            sender.tag = 10
            mapView.trafficEnabled = false
        }
    }
    
    @IBAction func nightMode(sender:UIButton) {
        if sender.tag == 10 {
            do {
                isNightMode = true
                routePolyline.strokeColor = UIColor.whiteColor()
                mapView.mapStyle = try GMSMapStyle(JSONString: kMapStyle)
                sender.tag = 20
                sender.setImage(UIImage(named: "sun"), forState: .Normal)
            } catch {
                NSLog("One or more of the map styles failed to load. \(error)")
            }
        }else{
            isNightMode = false
            routePolyline.strokeColor = UIColor.blueColor()
            trackDriverLocation = true
            sender.tag = 10
            sender.setImage(UIImage(named: "moon"), forState: .Normal)
            mapView.mapStyle = .None
        }
        
    }

    
    @IBAction func speak(sender: AnyObject) {
        
    }
    
    
    //MARK: Driver Route:
    func showRoute(sender:UIButton) {
        orderToBeDelivered = deliveryList[sender.tag]
        visualEffectView.frame = CGRectMake(0, minY, self.view.frame.size.width, self.view.frame.size.height)
        
        trackDriverLocation = true
        
        if locValue != nil {
            self.getDirections("\(locValue.latitude),\(locValue.longitude)", destination: orderToBeDelivered.shoplatLng, waypoints: nil, travelMode: nil, completionHandler: nil)
        }
        let camera = GMSCameraPosition.cameraWithTarget(locValue, zoom: 20, bearing: 270, viewingAngle: 45)
        mapView.camera = camera
        
        NSTimer.scheduledTimerWithTimeInterval(15.0, target: self, selector: #selector(updateDriverRoute(_:)), userInfo: nil, repeats: true)
    }
    
    func updateDriverRoute(sender:AnyObject) {
        
        if locValue != nil {
            self.getDirections("\(locValue.latitude),\(locValue.longitude)", destination: orderToBeDelivered.shoplatLng, waypoints: nil, travelMode: nil, completionHandler: nil)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locValue = manager.location!.coordinate
    }
    
    
    func mapView(mapView: GMSMapView, willMove gesture: Bool) {
        if gesture {
            trackDriverLocation =  trackDriverLocation ? false : true
        }
    }
    
    func mapView(mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
//        if marker.iconView ==  UIImageView(image: UIImage(named: "destinationPoint")) {
//            
//        }
        
        let view = UIView(frame: CGRectMake(0,0,75,50))
        let label = UILabel(frame: CGRectMake(0,0,75,20))
        label.text = "Hai"
        view.addSubview(label)
        return view
    }
    
    func getDirections(origin: String!, destination: String!, waypoints: Array<String>!, travelMode: AnyObject!, completionHandler: ((status: String, success: Bool) -> Void)?) {
        if let originLocation = origin {
            if let destinationLocation = destination {
                var directionsURLString = baseURLDirections + "origin=" + originLocation + "&destination=" + destinationLocation
                //                if let routeWaypoints = waypoints {
                //                    directionsURLString += "&waypoints=optimize:true"
                //
                //                    for waypoint in routeWaypoints {
                //                        directionsURLString += "|" + waypoint
                //                    }
                //                }
                //                print(directionsURLString)
                directionsURLString = directionsURLString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
                let directionsURL = NSURL(string: directionsURLString)
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    let directionsData = NSData(contentsOfURL: directionsURL!)
                    do{
                        let dictionary: Dictionary<NSObject, AnyObject> = try NSJSONSerialization.JSONObjectWithData(directionsData!, options: NSJSONReadingOptions.MutableContainers) as! Dictionary<NSObject, AnyObject>
                        
                        let status = dictionary["status"] as! String
                        if status == "ZERO_RESULTS" {
                            dispatch_async(dispatch_get_main_queue(), {
                                self.deliveryCount.text = "Driving Direction Not Available For : \n \(self.orderToBeDelivered.shopName)"
                                return
                                })
                        }
                        print(dictionary)
                        let route = dictionary["routes"] as! NSArray
                        let legs = route[0]["legs"] as! NSArray
                        let steps = legs.valueForKey("steps") as! NSArray
                        let current = steps[0] as! NSArray
                        
                        let str = String(current[0].valueForKey("html_instructions")!)
                        let distance = current[0].valueForKey("distance")! as! NSDictionary
                        let duration = current[0].valueForKey("duration")! as! NSDictionary
                        
                        
                        self.view.bringSubviewToFront(self.deliveryCount)
                        if status == "OK" {
                            self.selectedRoute = (dictionary["routes"] as! Array<Dictionary<NSObject, AnyObject>>)[0]
                            self.overviewPolyline = self.selectedRoute["overview_polyline"] as! Dictionary<NSObject, AnyObject>
                            
                            let legs = self.selectedRoute["legs"] as! Array<Dictionary<NSObject, AnyObject>>
                            
                            let startLocationDictionary = legs[0]["start_location"] as! Dictionary<NSObject, AnyObject>
                            self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
                            
                            let endLocationDictionary = legs[legs.count - 1]["end_location"] as! Dictionary<NSObject, AnyObject>
                            self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
                            
                            let originAddress = legs[0]["start_address"] as! String
                            //                            let destinationAddress = legs[legs.count - 1]["end_address"] as! String
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                if self.oldDirectionInstruction != str.plainTextFromHTML()! {
                                    let speechUtterance = AVSpeechUtterance(string: str.plainTextFromHTML()!)
                                    self.speechSynthesizer.speakUtterance(speechUtterance)
                                }
                                
                                
                                self.deliveryCount.text = "\(str.plainTextFromHTML()!) \n Distance: \(distance.valueForKey("text")!) \n Time: \(duration.valueForKey("text")!)"
                                
                                self.oldDirectionInstruction = str.plainTextFromHTML()!
                                
                                self.routePolyline.map = nil
                                self.originMarker.map = nil
                                self.destinationMarker.map = nil
                                
                                let van = self.isNightMode ? "nightDeliveryVan" : "deliveryVan"
                                self.originMarker = GMSMarker(position: self.originCoordinate)
                                self.originMarker.map = self.mapView
                                self.originMarker.iconView = UIImageView(image: UIImage(named: van))
                                self.originMarker.title = originAddress
                                
                                self.destinationMarker = GMSMarker(position: self.destinationCoordinate)
                                self.destinationMarker.map = self.mapView
                                self.destinationMarker.iconView = UIImageView(image: UIImage(named: "destinationPoint"))
                                
                                self.destinationMarker.title = self.orderToBeDelivered.shopName
                                self.destinationMarker.snippet = self.orderToBeDelivered.shopArea
                                
                                
                                //                            if waypoints != nil && waypoints.count > 0 {
                                //                                for waypoint in waypoints {
                                //                                    let lat: Double = (waypoint.componentsSeparatedByString(",")[0] as NSString).doubleValue
                                //                                    let lng: Double = (waypoint.componentsSeparatedByString(",")[1] as NSString).doubleValue
                                //
                                //                                    let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
                                //                                    marker.map = self.mapView
                                //                                    marker.icon = GMSMarker.markerImageWithColor(UIColor.purpleColor())
                                //
                                //                                }
                                //                            }
                                
                                let route = self.overviewPolyline["points"] as! String
                                
                                
                                let path: GMSPath = GMSPath(fromEncodedPath: route)!
                                self.routePolyline = GMSPolyline(path: path)
                                self.routePolyline.strokeColor = self.isNightMode ? UIColor.whiteColor() : UIColor.blueColor()
                                self.routePolyline.strokeWidth = 3.0
                                self.routePolyline.map = self.mapView
                            })
                            
                            
                        }
                        else {
                            //                            print("status")
                            //completionHandler(status: status, success: false)
                        }
                    }
                    catch {
                        //                        print("catch")
                        
                        // completionHandler(status: "", success: false)
                    }
                }
            }
            else {
                print("Destination is nil.")
                //completionHandler(status: "Destination is nil.", success: false)
            }
        }
        else {
            print("Origin is nil")
            //completionHandler(status: "Origin is nil", success: false)
        }
    }
    

    
    //MARK: UITableViewDataSource:
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deliveryList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:DeliveryCell = tableView.dequeueReusableCellWithIdentifier("DeliveryCell", forIndexPath: indexPath) as! DeliveryCell
        cell.deliveryAddress.text = deliveryList[indexPath.row].shopName
        cell.accept.addTarget(self, action: #selector(showRoute(_:)), forControlEvents: .TouchUpInside)
        cell.accept.tag = indexPath.row
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
}

class DeliveryCell: UITableViewCell {
    
    @IBOutlet var deliveryAddress: UILabel!
    @IBOutlet var accept: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension String {
    func plainTextFromHTML() -> String? {
        let regexPattern = "<.*?>"
        do {
            let stripHTMLRegex = try NSRegularExpression(pattern: regexPattern, options: NSRegularExpressionOptions.CaseInsensitive)
            let plainText = stripHTMLRegex.stringByReplacingMatchesInString(self, options: NSMatchingOptions.ReportProgress, range: NSMakeRange(0, self.characters.count), withTemplate: "")
            return plainText
        } catch {
            print("Warning: failed to create regular expression from pattern: \(regexPattern)")
            return nil
        }
    }
}