//
//  InvoiceComposer.swift
//  Print2PDF
//
//  Created by Gabriel Theodoropoulos on 23/06/16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

import UIKit

class InvoiceComposer: NSObject {

    let pathToInvoiceHTMLTemplate = NSBundle.mainBundle().pathForResource("invoice", ofType: "html")
    
    let pathToSingleItemHTMLTemplate = NSBundle.mainBundle().pathForResource("single_item", ofType: "html")
    
    let pathToLastItemHTMLTemplate = NSBundle.mainBundle().pathForResource("last_item", ofType: "html")
    
    let senderInfo = "Gabriel Theodoropoulos<br>123 Somewhere Str.<br>10000 - MyCity<br>MyCountry"
    
    let dueDate = ""
    
    let paymentMethod = "Wire Transfer"
    
    let logoImageURL = "https://goodlogo.com/images/logos/pepsico_logo_3845.gif"
    
    var invoiceNumber: String!
    
    var pdfFilename: String!
    
    
    override init() {
        super.init()
    }
    
    
    func renderInvoice(orderDetail:OrderDetailModel,orderList:[OrderModel]) -> String! {
        self.invoiceNumber = orderDetail.orderId
        
        do {
            var HTMLContent = try String(contentsOfFile: pathToInvoiceHTMLTemplate!)
            
            // The logo image.
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#LOGO_IMAGE#", withString: logoImageURL)
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#INVOICE_NUMBER#", withString: invoiceNumber)
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#INVOICE_DATE#", withString: orderDetail.orderDate)
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#DUE_DATE#", withString: dueDate)
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#SENDER_INFO#", withString: senderInfo)
            
            // Recipient info.
            let recipientInfo = "Owner: \(orderDetail.shopOwner) \n Shop Name: \(orderDetail.shopName) \n Shop Phone: \(orderDetail.shopPhone) \n Shop Area: \(orderDetail.shopArea)"
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#RECIPIENT_INFO#", withString: recipientInfo.stringByReplacingOccurrencesOfString("\n", withString: "<br>"))
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#TOTAL_AMOUNT#", withString: orderDetail.orderPrice)
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#PENDING_AMOUNT#", withString: orderDetail.pendingAmount == "" ? orderDetail.orderPrice : orderDetail.pendingAmount)
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#PAID_AMOUNT#", withString: orderDetail.paidAmount == "" ? "NA" : orderDetail.paidAmount)
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#CHEQUE_NUMBER#", withString: orderDetail.paymentMethod == .PayWithCash ? "NA" : orderDetail.chequeNumber != "" ? orderDetail.chequeNumber : "NA")
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#PAYMENT_METHOD#", withString: orderDetail.paidAmount == "" ? "NA" : orderDetail.paymentMethod == .PayWithCash ? "Cash" : "Cheque")
            
            var allItems = ""

            for i in 0..<orderList.count {
                let order = orderList[i]
                var itemHTMLContent: String!
                
                if i != orderList.count - 1 {
                    itemHTMLContent = try String(contentsOfFile: pathToSingleItemHTMLTemplate!)
                }
                else {
                    itemHTMLContent = try String(contentsOfFile: pathToLastItemHTMLTemplate!)
                }
                 itemHTMLContent = itemHTMLContent.stringByReplacingOccurrencesOfString("#ROW#", withString: String(i))
                
                 itemHTMLContent = itemHTMLContent.stringByReplacingOccurrencesOfString("#QTY#", withString: order.productQty)

                itemHTMLContent = itemHTMLContent.stringByReplacingOccurrencesOfString("#ITEM_NAME#", withString: order.productName)
                
                itemHTMLContent = itemHTMLContent.stringByReplacingOccurrencesOfString("#TOTALPRICE#", withString:order.productTotalPrice)
                
                itemHTMLContent = itemHTMLContent.stringByReplacingOccurrencesOfString("#TAX#", withString:order.productTax)
                
                itemHTMLContent = itemHTMLContent.stringByReplacingOccurrencesOfString("#DISCOUNT#", withString:order.productDisc)
                
                itemHTMLContent = itemHTMLContent.stringByReplacingOccurrencesOfString("#PRICE#", withString: order.productPrice)
                
                allItems += itemHTMLContent
            }
            
            HTMLContent = HTMLContent.stringByReplacingOccurrencesOfString("#ITEMS#", withString: allItems)
            
            return HTMLContent
            
        }
        catch {
            print("Unable to open and use HTML template files.")
        }
        
        return nil
    }
    
    
    func exportHTMLContentToPDF(HTMLContent: String) {
        let printPageRenderer = CustomPrintPageRenderer()
        
        let printFormatter = UIMarkupTextPrintFormatter(markupText: HTMLContent)
        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAtIndex: 0)
        
        let pdfData = drawPDFUsingPrintPageRenderer(printPageRenderer)
        
        pdfFilename = "\(AppDelegate.getAppDelegate().getDocDir())/Invoice\(invoiceNumber).pdf"
        pdfData.writeToFile(pdfFilename, atomically: true)
        
        print(pdfFilename)
    }
    
    
    func drawPDFUsingPrintPageRenderer(printPageRenderer: UIPrintPageRenderer) -> NSData! {
        let data = NSMutableData()
        
        UIGraphicsBeginPDFContextToData(data, CGRectZero, nil)
        
        UIGraphicsBeginPDFPage()
        
        printPageRenderer.drawPageAtIndex(0, inRect: UIGraphicsGetPDFContextBounds())
        
        UIGraphicsEndPDFContext()
        
        return data
    }
    
}
