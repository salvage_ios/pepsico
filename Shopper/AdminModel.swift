//
//  AdminModel.swift
//  Shopper
//
//  Created by VividMacmini7 on 29/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FirebaseDatabase

struct AdminModel {
    let key:String
    let adminName:String
    let adminEmail:String
    let adminPassword:String
    let adminPhonenumber:String
    let accountActivated:Bool
    
    let ref: FIRDatabaseReference?
    
    init(name:String,email:String, password:String, phone:String, status:Bool , key:String = "") {
        self.key = key
        self.adminName = name
        self.adminEmail = email
        self.adminPassword = password
        self.adminPhonenumber = phone
        self.accountActivated = status
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        adminName = snapshotValue["admin_name"] as! String
        adminEmail = snapshotValue["admin_email"] as! String
        adminPassword = snapshotValue["admin_password"] as! String
        adminPhonenumber = snapshotValue["admin_phone"] as! String
        accountActivated = snapshotValue["account_status"] as! Bool
        ref = snapshot.ref
    }
    
    func toAnyObject() -> AnyObject {
        return [
            "admin_name": adminName,
            "admin_email": adminEmail,
            "admin_password": adminPassword,
            "admin_phone": adminPhonenumber,
            "account_status": accountActivated,
        ]
    }
}
