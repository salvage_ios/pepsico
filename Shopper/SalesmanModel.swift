//
//  SalesmanModel.swift
//  Shopper
//
//  Created by VividMacmini7 on 28/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FirebaseDatabase

struct SalesmanModel {
    let key:String
    let salesmanName:String
    let salesmanPhone:String
    let salesmanAddress:String
    let salesmanEmail:String
    let salesmanPassword:String
    var workArea:[String]?
    let ref: FIRDatabaseReference?
    
    init(name:String, phone:String, address:String, email:String, password:String, workareas:[String], key:String = "") {
        self.key = key
        self.salesmanName = name
        self.salesmanPhone = phone
        self.salesmanAddress = address
        self.salesmanEmail = email
        self.salesmanPassword = password
        self.workArea = workareas
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        salesmanName = snapshotValue["salesman_name"] as! String
        salesmanPhone = snapshotValue["salesman_phone"] as! String
        salesmanAddress = snapshotValue["salesman_address"] as! String
        salesmanEmail = snapshotValue["salesman_email"] as! String
        salesmanPassword = snapshotValue["salesman_password"] as! String
        workArea = snapshotValue["work_area"] as? [String]
        ref = snapshot.ref
    }
    
    func toAnyObject() -> AnyObject {
        return [
            "salesman_name": salesmanName,
            "salesman_phone": salesmanPhone,
            "salesman_address": salesmanAddress,
            "salesman_email": salesmanEmail,
            "salesman_password": salesmanPassword,
            "work_area": workArea!
        ]
    }
}
