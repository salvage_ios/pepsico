//
//  Loign.swift
//  Shopper
//
//  Created by VividMacmini7 on 29/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class Loign: UIViewController {

    @IBOutlet var userEmail:UITextField!
    @IBOutlet var userPassword:UITextField!
    
    var adminRef = FIRDatabase.database().referenceWithPath("admin_list")
    let salesmanRef = FIRDatabase.database().referenceWithPath("salesman_list")
    let userRef = FIRDatabase.database().referenceWithPath("user_list")
    
    let loggedInUserId = ""
    var salesmanKeys = [String]()
    var adminKeys = [String]()
    var allAdminUsers = [AdminModel]()
    var allSalesmanUsers = [SalesmanModel]()
    var userId = ""
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userEmail.text = "gokulece26@gmail.com"
        self.userPassword.text = "123456"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: IBAction
    @IBAction func login(sender:UIButton) {
        if validatLogin() {
            print("\(userEmail.text!) \(userPassword.text!)")
            FIRAuth.auth()?.signInWithEmail(userEmail.text!, password: userPassword.text!, completion: { (user, error) in
                if error  == nil {
                    self.userId = (FIRAuth.auth()?.currentUser?.uid)!
                    self.userRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
                        if !(snapshot.value is NSNull) {
                            for child in snapshot.children {
                                let user = UserModel(snapshot: child as! FIRDataSnapshot)
                                if user.key == self.userId {
                                    if user.uType == .UserTypAdmin { //Admin
                                        self.adminRef = self.adminRef.child(self.userId)
                                        self.adminRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
                                            if !(snapshot.value is NSNull) {
                                                let admin = AdminModel(snapshot: snapshot )
                                                if admin.accountActivated{
                                                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("DashBoard") as! DashBoard
                                                    vc.userId = self.userId
                                                    let nacvc = UINavigationController(rootViewController: vc)
                                                    self.appDelegate.window?.rootViewController = nacvc
                                                    
                                                    self.storeUserDetailsInUserDefault(user)
                                                }else{
                                                    HelperClass.showSimpleAlertWith("Need approval from admin, \nPlease contact your owner for more details", inVC: self)
                                                }
                                            }else{
                                                HelperClass.showSimpleAlertWith("Something went wrong, \n Please try again.", inVC: self)
                                            }
                                        })
                                        
                                    }else if user.uType == .UserTypeSalesman { //Salesman
                                        
                                        self.storeUserDetailsInUserDefault(user)
                                        
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SalesmanProfile") as! SalesmanProfile
                                        let nacvc = UINavigationController(rootViewController: vc)
                                        self.appDelegate.window?.rootViewController = nacvc
                                        
                                    }else if user.uType == .UserTypeDriver { //Driver
                                        
                                        self.storeUserDetailsInUserDefault(user)
                                        
                                        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("DriverProfile") as! DriverProfile
                                        let nacvc = UINavigationController(rootViewController: vc)
                                        self.appDelegate.window?.rootViewController = nacvc
                                    }
                                    
                                    
                                    
                                    
                                }
                            }
//                            self.geAllSalesmanUserKeeys()
                        }
                    })
//                    self.getAllAdminUserKeys() //Get all user keys
                }else{
                    HelperClass.showSimpleAlertWith((error?.localizedDescription)!, inVC: self)
                }
            })
        }else{
            HelperClass.showSimpleAlertWith("Username and password are mandatory", inVC: self)
        }
    }
    
    @IBAction func signUp(sender:UIButton) {
        let alert = UIAlertController(title: "Sign up", message: "Account will be activated after admin approval only", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "User name"
            textfield.autocapitalizationType = .Words
        }
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Email Id"
            textfield.keyboardType = .EmailAddress
        }
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Password"
            textfield.secureTextEntry = true
        }
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Confirm Password"
            textfield.secureTextEntry = true
        }
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "PhoneNumber"
            textfield.keyboardType = .PhonePad
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Signup", style: .Default, handler: { (action) in
            if self.validatSignup(alert) {
                FIRAuth.auth()?.createUserWithEmail(alert.textFields![1].text!, password: alert.textFields![2].text!, completion: { (user, error) in
                    if error == nil {
                        print("You have successfully signed up")
                        self.addAdminDetails((user?.uid)!, alert: alert)
                        
                        let userModel = UserModel(id: (user?.uid)!, name: alert.textFields![0].text!, phone: alert.textFields![4].text!, email: alert.textFields![1].text!, type: .UserTypAdmin)
                        
                        let itemRef = self.userRef.child((user!.uid))
                        itemRef.setValue(userModel.toAnyObject())
                        
                        
                    } else {
                        let failureAlert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .Alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                        failureAlert.addAction(defaultAction)
                        self.presentViewController(failureAlert, animated: true, completion: nil)
                    }
                })
            }
        }))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func forgetPassword(sender:UIButton) {
        let alert = UIAlertController(title: "Forget password?", message: "Enter your registered email id", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Registered Email"
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Send", style: .Default, handler: { (action) in
           let textfield = alert.textFields![0]
            print(textfield.text)
            if alert.textFields![0].text == "" {
                HelperClass.showSimpleAlertWith("Please enter an valid email address.", inVC: self)
            } else {
                FIRAuth.auth()?.sendPasswordResetWithEmail(alert.textFields![0].text!, completion: { (error) in
                    if error == nil {
                        HelperClass.showSimpleAlertWith("Reset password link has been sent to your email", inVC: self)
                    }
                })
            }
        }))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
    
//    //MARK: LoginValidation
//    func getAllAdminUserKeys(){
//        adminRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
//            if !(snapshot.value is NSNull) {
//                for child in snapshot.children {
//                    let admin = AdminModel(snapshot: child as! FIRDataSnapshot)
//                    self.adminKeys.append(admin.key)
//                    self.allAdminUsers.append(admin)
//                }
//                self.geAllSalesmanUserKeeys()
//            }
//        })
//        
//        
//        
//        
//    }
//    
//    func geAllSalesmanUserKeeys() {
//        salesmanRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
//            if !(snapshot.value is NSNull) {
//                for child in snapshot.children {
//                    let salesman = SalesmanModel(snapshot: child as! FIRDataSnapshot)
//                    self.salesmanKeys.append(salesman.key)
//                    self.allSalesmanUsers.append(salesman)
//                }
//                self.checkLogin()
//            }
//        })
//    }
//    
//    func checkLogin(type:UserType) {
//        print(salesmanKeys)
//        print(self.userId)
//        if type == .UserTypAdmin {
//            //user is admin
//            let index = self.adminKeys.indexOf(self.userId)
//            let loggedAdmin = self.allAdminUsers[index!]
//            if loggedAdmin.accountActivated {
//                
//                let model = UserModel(id: loggedAdmin.key, name: loggedAdmin.adminName, phone: loggedAdmin.adminPhonenumber, email: loggedAdmin.adminEmail, type: .UserTypAdmin)
//                print(model.toAnyObject())
//                NSUserDefaults.standardUserDefaults().setObject(model.toAnyObject() , forKey: loggedAdmin.key)
//                NSUserDefaults.standardUserDefaults().setValue(loggedAdmin.key, forKey: loggdInUser)
//                NSUserDefaults.standardUserDefaults().synchronize()
//                
//                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("DashBoard") as! DashBoard
//                vc.userId = loggedAdmin.key
//                let nacvc = UINavigationController(rootViewController: vc)
//                appDelegate.window?.rootViewController = nacvc
//
//            }else{
//                HelperClass.showSimpleAlertWith("Need approval from admin, \nPlease contact your owner for more details", inVC: self)
//            }
//            
//        }else if type == .UserTypeSalesman  {
//            //user is salesman
//            let index = self.salesmanKeys.indexOf(self.userId)
//            let loggedSalesman = self.allSalesmanUsers[index!]
//            
//            let model = UserModel(id: loggedSalesman.key, name: loggedSalesman.salesmanName, phone: loggedSalesman.salesmanPhone, email: loggedSalesman.salesmanEmail, type: .UserTypeSalesman)
//            print(model.toAnyObject())
//            NSUserDefaults.standardUserDefaults().setObject(model.toAnyObject() , forKey: loggedSalesman.key)
//            NSUserDefaults.standardUserDefaults().setValue(loggedSalesman.key, forKey: loggdInUser)
//            NSUserDefaults.standardUserDefaults().synchronize()
//            
//            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SalesmanProfile") as! SalesmanProfile
//            vc.salesmanDetails = loggedSalesman
//            let nacvc = UINavigationController(rootViewController: vc)
//            appDelegate.window?.rootViewController = nacvc
//        }
//    }
    

    
    //MARK: Support:
    
    func storeUserDetailsInUserDefault(user:UserModel){
        NSUserDefaults.standardUserDefaults().setObject(user.toAnyObject() , forKey: user.key)
        NSUserDefaults.standardUserDefaults().setValue(user.key, forKey: loggdInUser)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func addAdminDetails(uid:String,alert:UIAlertController) {
        
        let salesmanDetails = AdminModel(name:alert.textFields![0].text! , email: alert.textFields![1].text!, password: alert.textFields![2].text!, phone: alert.textFields![4].text!, status: false)
        
        let itemRef = self.adminRef.child(uid)
        itemRef.setValue(salesmanDetails.toAnyObject())
        
        HelperClass.showSimpleAlertWith("Signup successful \n Account will be activated after admin approval", inVC: self)
    }
    func validatLogin() -> Bool {
        if FIRAuth.auth()?.currentUser != nil {
            do {
                try FIRAuth.auth()?.signOut()
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        if userEmail.text != "" && userPassword.text != "" {
            return true
        }
        return false
    }
    
    func validatSignup(alert:UIAlertController) -> Bool {
        if alert.textFields![0].text != "" &&
            alert.textFields![1].text != "" &&
            alert.textFields![2].text != "" &&
            alert.textFields![3].text != "" &&
            alert.textFields![4].text != "" &&
            (alert.textFields![2].text == alert.textFields![3].text) {
            return true
        }else if (alert.textFields![2].text != alert.textFields![3].text) {
            HelperClass.showSimpleAlertWith("Password and Confirm password does't match each other.", inVC: self)
        }else{
            HelperClass.showSimpleAlertWith("Validation failed \n Make sure all fields are filled.", inVC: self)
        }
        
        return false
    }
    
}
