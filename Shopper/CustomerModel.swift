//
//  CustomerModel.swift
//  Shopper
//
//  Created by VividMacmini7 on 27/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FirebaseDatabase

struct CustomerModel {
    let key:String
    let customerName:String
    let customerPhone:String
    let shopName:String
    let shopAddress:String
    let shopArea:String
    let shopCityState:String
    let shopZipCode:String
    let shopLatLng:String
    var orderList:[String]?
    let ref: FIRDatabaseReference?
    
    init(name:String, phone:String, shopName:String, address:String, shopArea:String,cityState:String,Zip:String,orderList:[String],latlng:String, key:String = "") {
        self.key = key
        self.customerName = name
        self.customerPhone = phone
        self.shopName = shopName
        self.shopAddress = address
        self.shopArea = shopArea
        self.orderList = orderList
        self.shopCityState = cityState
        self.shopZipCode = Zip
        shopLatLng = latlng
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        customerName = snapshotValue["customer_name"] as! String
        customerPhone = snapshotValue["customer_phone"] as! String
        shopName = snapshotValue["shop_name"] as! String
        shopAddress = snapshotValue["shop_address"] as! String
        shopArea = snapshotValue["shop_area"] as! String
        orderList = snapshotValue["order_list"] as? [String]
        shopLatLng = snapshotValue["shop_latlng"] as! String
        shopCityState = snapshotValue["shop_cityState"] as! String
        shopZipCode = snapshotValue["shop_zip"] as! String
        ref = snapshot.ref
    }
    
    func toAnyObject() -> AnyObject {
        return [
            "customer_name": customerName,
            "customer_phone": customerPhone,
            "shop_name": shopName,
            "shop_address": shopAddress,
            "shop_area": shopArea ,
            "shop_latlng" : shopLatLng ,
            "shop_cityState" : shopCityState ,
            "shop_zip" : shopZipCode ,
            "order_list" : orderList!
        ]
    }
}
