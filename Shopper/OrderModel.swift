//
//  OrderModel.swift
//  Shopper
//
//  Created by VividMacmini7 on 31/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct OrderModel {
    let key:String
    
    var productName:String
    var productPrice:String
    var productQty:String
    var productTax:String
    var productDisc:String
    var productTotalPrice:String
    
    
    let ref: FIRDatabaseReference?
    
    init(productName:String, productPrice:String, productQty:String, productTotlaPrice:String, productTax:String, productDisc:String,  key:String = "") {
        self.key = key
        self.productQty = productQty
        self.productTotalPrice = productTotlaPrice
        self.productName = productName
        self.productTax = productTax
        self.productDisc = productDisc
        self.productPrice = productPrice
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        self.productQty = snapshotValue["product_qty"] as! String
        self.productTotalPrice = snapshotValue["product_totalPrice"] as! String
        self.productName = snapshotValue["product_name"] as! String
        self.productPrice = snapshotValue["product_price"] as! String
        self.productTax = snapshotValue["product_tax"] as! String
        self.productDisc = snapshotValue["product_disc"] as! String
        self.ref = snapshot.ref
    }
    
    func toAnyObject() -> AnyObject {
        return [
            "product_qty": productQty,
            "product_totalPrice": productTotalPrice,
            "product_name": productName,
            "product_tax": productTax,
            "product_disc": productDisc,
            "product_price": productPrice,
        ]
    }
}