//
//  OrderListModel.swift
//  Shopper
//
//  Created by VividMacmini7 on 31/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct OrderDetailModel {
    let key:String
    var orderId:String
    var orderQty:String
    var orderPrice:String
    var orderTax:String
    var orderDiscount:String
    
    var orderTakenBy:String
    var orderPackedBy:String
    var orderDeliveredBy:String
    var orderDate:String
    var orderStatus:OrderStatus
    
    var shopName:String
    var shopPhone:String
    var shopOwner:String
    var shopArea:String
    var shoplatLng:String
    
    var paidAmount:String
    var pendingAmount:String
    var paymentMethod:PaymentMethod
    var chequeNumber:String
    var chequeDate:String
    var bankName:String
    
   
    
    let ref: FIRDatabaseReference?
    
    init(id:String,qty:String, price:String, tax:String = "", discount:String = "", takenBy:String, packedBy:String = "", deliveredBy:String = "", date:String , shopName:String, shopPhone:String, shopOwner:String,shopArea:String, orderStatus:OrderStatus, paidAmount:String = "", pendingAmount:String = "",paymentMethod:PaymentMethod = PaymentMethod.NotPaid, chequeNumber:String = "", chequeDate:String = "", bankName:String = "", key:String = "",latLng:String) {
        self.key = key
        self.orderId = id
        self.orderQty = qty
        
        self.orderPrice = price
        self.orderTax = tax
        self.orderDiscount = discount
        
        self.orderTakenBy = takenBy
        self.orderPackedBy = packedBy
        self.orderDeliveredBy = deliveredBy
        
        self.orderDate = date
        self.orderStatus = orderStatus
        
        self.shopName = shopName
        self.shopPhone = shopPhone
        self.shopOwner = shopOwner
        self.shopArea = shopArea
        self.shoplatLng = latLng
        
        self.paidAmount = paidAmount
        self.pendingAmount = pendingAmount
        self.paymentMethod = paymentMethod
        self.chequeNumber = chequeNumber
        self.chequeDate = chequeDate
        self.bankName = bankName
        
        
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        self.orderId = snapshotValue["order_id"] as! String
        self.orderQty = snapshotValue["order_qty"] as! String
        
        self.orderPrice = snapshotValue["order_price"] as! String
        self.orderTax = snapshotValue["order_tax"] as! String
        self.orderDiscount = snapshotValue["order_disc"] as! String
        
        self.orderTakenBy = snapshotValue["order_takenBy"] as! String
        self.orderPackedBy = snapshotValue["order_packedBy"] as! String
        self.orderDeliveredBy = snapshotValue["order_deliveredBy"] as! String
        
        
        self.orderDate = snapshotValue["order_date"] as! String
        
        let type = snapshotValue["order_status"] as! OrderStatus.RawValue
        switch type {
        case 1:
            orderStatus = .OrderTaken
            break
        case 2:
            orderStatus = .OrderPacked
            break
        case 3:
            orderStatus = .OrderDelivered
            break
        default:
            orderStatus = .NoOrder
            break
        }
        
        
        self.shopName = snapshotValue["shop_name"] as! String
        self.shopPhone = snapshotValue["shop_phone"] as! String
        self.shopOwner = snapshotValue["shop_owner"] as! String
        self.shopArea = snapshotValue["shop_area"] as! String
        self.shoplatLng = snapshotValue["shop_latlng"] as! String
        
        self.paidAmount = snapshotValue["paid_amount"] as! String
        self.pendingAmount = snapshotValue["pending_amount"] as! String
        self.chequeNumber = snapshotValue["cheque_number"] as! String
        self.chequeDate = snapshotValue["cheque_date"] as! String
        self.bankName = snapshotValue["bank_name"] as! String
        
        let paymentType = snapshotValue["payment_method"] as! PaymentMethod.RawValue
        switch paymentType {
        case 0:
            paymentMethod = .NotPaid
            break
        case 1:
            paymentMethod = .PayWithCash
            break
        case 2:
            paymentMethod = .PayWithCheque
            break
        default:
            paymentMethod = .NotPaid
            break
        }
        
        self.ref = snapshot.ref
    }
    
    func toAnyObject() -> AnyObject {
        return [
            "order_id": orderId,
            "order_qty": orderQty,
            "order_price": orderPrice,
            "order_tax": orderTax,
            "order_disc": orderDiscount,
            
            "order_takenBy": orderTakenBy,
            "order_packedBy" : orderPackedBy ,
            "order_deliveredBy" : orderDeliveredBy ,
            
            "order_date": orderDate,
            "order_status": orderStatus.rawValue ,

            "shop_name": shopName,
            "shop_phone": shopPhone,
            "shop_owner": shopOwner,
            "shop_area": shopArea,
            "shop_latlng": shoplatLng,
            
            "paid_amount": paidAmount,
            "pending_amount": pendingAmount,
            "cheque_number": chequeNumber,
            "cheque_date": chequeDate,
            "bank_name": bankName,
            "payment_method": paymentMethod.rawValue
        ]
    }
}