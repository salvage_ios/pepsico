//
//  AreaList.swift
//  Shopper
//
//  Created by VividMacmini7 on 27/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase

class AreaList: UITableViewController {

    var previousVC:UIViewController!
    let areaRef = FIRDatabase.database().referenceWithPath("area_list")
    let salesmanRef = FIRDatabase.database().referenceWithPath("salesman_list")
    let driverRef = FIRDatabase.database().referenceWithPath("driver_list")
    let customerRef = FIRDatabase.database().referenceWithPath("customer_list")
    
    var areaList = [AreaModule]()
    var selectedArea = ""
    var multipleAreas = [String]()

    var multipleAreaModel = [AreaModule]()
    var salesman = [String]()
    var driverList = [String]()
    var selecteduserProfile = "NA"
    var isSelectDriverArea = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        
        let n: Int! = self.navigationController?.viewControllers.count
        let viewControllers = self.navigationController?.viewControllers
        if viewControllers!.count < 2 { //Means no previous VC
            let rightBarButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(selectArea(_:)))
            self.navigationItem.rightBarButtonItem = rightBarButton
            
            let leftBarButton = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(cancel(_:)))
            self.navigationItem.leftBarButtonItem = leftBarButton
            return
        }
        previousVC = viewControllers![n-2]
        
        if previousVC! is DashBoard  {
            let rightBarButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(addNewArea(_:)))
            self.navigationItem.rightBarButtonItem = rightBarButton
        }else if previousVC! is AddSalesman  {
            let rightBarButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(selectArea(_:)))
            self.navigationItem.rightBarButtonItem = rightBarButton
            
            let leftBarButton = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(cancel(_:)))
            self.navigationItem.leftBarButtonItem = leftBarButton
        }
    }

    override func viewWillAppear(animated: Bool) {
        loadAreaList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    
    //MARK: IBAction:
    @IBAction func addNewArea(sender: UIButton) {

        let altMessage = UIAlertController(title: "Add Area?", message: "Enter new area name", preferredStyle: UIAlertControllerStyle.Alert)
        altMessage.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        altMessage.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action) in
            guard let name = altMessage.textFields![0].text else { return }
            let areaName = AreaModule(areaName: name, salesman: "", salesmanid: "")
            let itemRef = self.areaRef.child(name.lowercaseString)
            itemRef.setValue(areaName.toAnyObject())
            dispatch_async(dispatch_get_main_queue(), {
                self.loadAreaList()
            })
        }))
        altMessage.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Area Name"
            textfield.autocapitalizationType = .Words
        }
        self.presentViewController(altMessage, animated: true, completion: nil)
    }
    
    @IBAction func selectArea(sender: UIButton) {
        if previousVC == nil {
            NSNotificationCenter.defaultCenter().postNotificationName("keyAreaSelected", object: nil, userInfo: ["area":selectedArea])
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
            
        }else{
            multipleAreas = Array(Set(multipleAreas))
            NSNotificationCenter.defaultCenter().postNotificationName("keyWorkAreaSelected", object: nil, userInfo: ["area":multipleAreas])
            if previousVC! is AddSalesman {
                self.navigationController?.popViewControllerAnimated(true)
                return
            }
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    
    @IBAction func cancel(sender: UIButton) {
        if previousVC == nil {
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
            return
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if !isSelectDriverArea {
            for area in areaList {
                salesman.append(area.salesman.characters.count != 0 ? area.salesman : "Salesman Not Allocated" )
            }
            salesman = Array(Set(salesman))
            return salesman.count != 0 ? salesman.count : 1
        }
        for area in areaList {
            driverList.append(area.driver.characters.count != 0 ? area.driver : "Driver Not Allocated" )
        }
        driverList = Array(Set(driverList))
        return driverList.count != 0 ? driverList.count : 1
        
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isSelectDriverArea {
            var idx = 0
            var NA = 0
            for area in areaList {
                if area.salesman == salesman[section] {
                    idx += 1
                }
                NA += area.salesman.characters.count != 0 ? 0 : 1
            }
            return idx != 0 ? idx : NA
        }
        var idx = 0
        var NA = 0
        for area in areaList {
            if area.driver == driverList[section] {
                idx += 1
            }
            NA += area.driver.characters.count != 0 ? 0 : 1
        }
        return idx != 0 ? idx : NA
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        var idx = 0
        var notAssignerArea = [AreaModule]()
        var otherAreas = [AreaModule]()
        for area in areaList {
            if !isSelectDriverArea {
                if area.salesman == salesman[indexPath.section] {
                    idx += 1
                    otherAreas.append(area)
                }
                
                if area.salesman.characters.count == 0 {
                    notAssignerArea.append(area)
                }
            }else{
                if area.driver == driverList[indexPath.section] {
                    idx += 1
                    otherAreas.append(area)
                }
                
                if area.driver.characters.count == 0 {
                    notAssignerArea.append(area)
                }
            }
            
        }
        if idx == 0 {
            cell.textLabel?.text = notAssignerArea[indexPath.row].areaName
        }else{
            cell.textLabel?.text = otherAreas[indexPath.row].areaName
            if multipleAreas.contains(otherAreas[indexPath.row].areaName) {
                cell.accessoryType = .Checkmark
            }else{
                cell.accessoryType = .None
            }
        }
        
        
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if previousVC is DashBoard {
            
        }else if previousVC == nil {
            self.tableView.reloadData()
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            cell?.accessoryType = .Checkmark
            selectedArea = (cell?.textLabel?.text)!
            
        }else{
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            if !multipleAreas.contains((cell?.textLabel?.text)!) {
                multipleAreas.append((cell?.textLabel?.text)!)
                cell?.accessoryType = .Checkmark
            }else{
                let index = multipleAreas.indexOf((cell?.textLabel?.text)!)
                multipleAreas.removeAtIndex(index!)
                cell?.accessoryType = .None
            }
        }

    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return previousVC is DashBoard ? true : false
    }
    
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let moreRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Edit", handler:{action, indexpath in
            var allareaName = [String]()
            for area in self.areaList {
                allareaName.append(area.areaName)
            }
            let cell = tableView.cellForRowAtIndexPath(indexPath)
            let index = allareaName.indexOf((cell?.textLabel?.text)!)
            
            let area = self.areaList[index!]
           self.editArea(area)
        });
        moreRowAction.backgroundColor = UIColor(red: 0.298, green: 0.851, blue: 0.3922, alpha: 1.0);
        
        let deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete", handler:{action, indexpath in
            let alert = UIAlertController(title: "Warning", message: "Deleting area will result in deleting customers from that area", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Delete", style: .Destructive, handler: { (action) in
                var allareaName = [String]()
                for area in self.areaList {
                    allareaName.append(area.areaName)
                }
                
                let cell = tableView.cellForRowAtIndexPath(indexPath)
                let index = allareaName.indexOf((cell?.textLabel?.text)!)
                
                let area = self.areaList[index!]
                print("-------Deleted Area: \(area.areaName)-------")
                self.updateSalesmanArea(area.areaName)
                self.removeCustomersfromThis(area.areaName)
                area.ref?.removeValue()
                self.areaList.removeAtIndex(indexPath.row)
                self.title = "Area (\(self.areaList.count))"
                self.tableView.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
            dispatch_async(dispatch_get_main_queue(), { 
                self.navigationController?.presentViewController(alert, animated: true, completion: nil)
            })
            
        });
        
        return [deleteRowAction, moreRowAction];
    }
   
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if salesman.count != 0 || driverList.count != 0 {
            var idx = 0
            var NA = 0
            var item = [String]()
            if !isSelectDriverArea {
                item = salesman
                for area in areaList {
                    if area.salesman == item[section] {
                        idx += 1
                    }
                    NA += area.salesman.characters.count != 0 ? 0 : 1
                }
            }else{
                item = driverList
                for area in areaList {
                    if area.driver == item[section] {
                        idx += 1
                    }
                    NA += area.salesman.characters.count != 0 ? 0 : 1
                }
            }
            
            
            
            let headerView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,40))
            headerView.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
            let areaName = UILabel(frame:  CGRectMake(15,0,self.view.frame.size.width/2,40))
            areaName.text = item[section]
            areaName.textColor = idx == 0 ? UIColor.redColor() : UIColor.blackColor()
            areaName.font = UIFont.AppFontBold()
            headerView.addSubview(areaName)
            
            
            let salesMan = UILabel(frame:  CGRectMake((self.view.frame.size.width/2) - 15,0,self.view.frame.size.width/2,40))
            salesMan.text = "(\(idx == 0 ? NA : idx))"
            salesMan.textColor = UIColor.blackColor()
            salesMan.textAlignment = .Right
            salesMan.font = UIFont.AppFontBold()
            headerView.addSubview(salesMan)
            
            return headerView
        }
        return UIView()
    }

    
    //MARK: Support
    func editArea(sender:AreaModule) {
        let altMessage = UIAlertController(title: "Edit Area?", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        altMessage.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        altMessage.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action) in
            guard let name = altMessage.textFields![0].text else {
                HelperClass.showSimpleAlertWith("Enter Area Name", inVC: self)
                return
            }
            sender.ref?.updateChildValues(["area_name":name])
            dispatch_async(dispatch_get_main_queue(), { 
                self.loadAreaList()
            })
            
        }))
        altMessage.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Area Name"
            textfield.text = sender.areaName
        }
        self.presentViewController(altMessage, animated: true, completion: nil)
    }
    func removeCustomersfromThis(area:String) {
        customerRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if !(snapshot.value is NSNull) {
                for child in snapshot.children {
                    let shop = CustomerModel(snapshot: child as! FIRDataSnapshot)
                    if shop.shopArea == area {
                        shop.ref?.removeValue()
                    }
                    
                }
            }
        })
    }
    func loadAreaList() {
        areaList = [AreaModule]()
        areaRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                let backgroundImage = UIImage(named: "TableBG1")
                let imageView = UIImageView(image: backgroundImage)
                self.tableView.backgroundView = imageView
                return
            } else {
                self.tableView.backgroundView = UIView()
                for child in snapshot.children {
                    let area = AreaModule(snapshot: child as! FIRDataSnapshot)
                    if !self.isSelectDriverArea && area.salesman == self.selecteduserProfile {
                        self.multipleAreas.append(area.areaName)
                        
                    }else if self.isSelectDriverArea && area.driver == self.selecteduserProfile {
                        self.multipleAreas.append(area.areaName)
                    }
                    self.areaList.append(area)
                }
                dispatch_async(dispatch_get_main_queue(), {
                    if self.areaList.count == 0 {
                        let backgroundImage = UIImage(named: "TableBG1")
                        let imageView = UIImageView(image: backgroundImage)
                        self.tableView.backgroundView = imageView
                        return
                    }
                    self.tableView.reloadData()
                    self.title = "Area (\(self.areaList.count))"
                })
                
            }
        })
    }

    //After deleting area from DB.Update salesman area list
    func updateSalesmanArea(deletedArea:String) {
        var salesmanArray = [SalesmanModel]()
        salesmanRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let salesman = SalesmanModel(snapshot: child as! FIRDataSnapshot)
                    salesmanArray.append(salesman)
                }
                for salesman in salesmanArray {
                    var workArea = salesman.workArea
                    if workArea != nil {
                        if workArea!.contains(deletedArea) {
                            let index = workArea!.indexOf(deletedArea)
                            workArea?.removeAtIndex(index!)
                            salesman.ref?.updateChildValues([
                                "work_area": workArea!
                                ])
                        }
                    }
                    
                }
            }
        })
        
        
    }
}
