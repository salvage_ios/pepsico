
//
//  DriverModel.swift
//  Shopper
//
//  Created by VividMacmini7 on 05/04/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FirebaseDatabase

struct DriverModel {
    let key:String
    let driverName:String
    let driverPhone:String
    let driverAddress:String
    let driverEmail:String
    let driverPassword:String
    var workArea:[String]?
    let ref: FIRDatabaseReference?
    
    init(name:String, phone:String, address:String, email:String, password:String, workareas:[String], key:String = "") {
        self.key = key
        self.driverName = name
        self.driverPhone = phone
        self.driverAddress = address
        self.driverEmail = email
        self.driverPassword = password
        self.workArea = workareas
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        driverName = snapshotValue["driver_name"] as! String
        driverPhone = snapshotValue["driver_phone"] as! String
        driverAddress = snapshotValue["driver_address"] as! String
        driverEmail = snapshotValue["driver_email"] as! String
        driverPassword = snapshotValue["driver_password"] as! String
        workArea = snapshotValue["work_area"] as? [String]
        ref = snapshot.ref
    }
    
    func toAnyObject() -> AnyObject {
        return [
            "driver_name": driverName,
            "driver_phone": driverPhone,
            "driver_address": driverAddress,
            "driver_email": driverEmail,
            "driver_password": driverPassword,
            "work_area": workArea!
        ]
    }
}
