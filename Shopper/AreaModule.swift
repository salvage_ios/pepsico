//
//  AreaModule.swift
//  Shopper
//
//  Created by VividMacmini7 on 27/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FirebaseDatabase

struct AreaModule {
    let key:String
    let areaName:String
    let salesman:String
    let salesmanUid:String
    let driver:String
    let driverUid:String
    
    let ref: FIRDatabaseReference?
    
    init(areaName:String,salesman:String,salesmanid:String, driver:String = "", driverid:String = "", key:String = "") {
        self.key = key
        self.areaName = areaName
        self.salesman = salesman
        self.salesmanUid = salesmanid
        self.driver = driver
        self.driverUid = driverid
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        key = snapshot.key
        let snapshotValue = snapshot.value as! [String: AnyObject]
        areaName = snapshotValue["area_name"] as! String
        salesman = snapshotValue["salesman"] as! String
        salesmanUid = snapshotValue["salesman_id"] as! String
        driver = snapshotValue["driver"] as! String
        driverUid = snapshotValue["driver_id"] as! String
        ref = snapshot.ref
    }
    
    func toAnyObject() -> AnyObject {
        return [
            "area_name": areaName ,
            "salesman": salesman ,
            "salesman_id": salesmanUid ,
            "driver" : driver,
            "driver_id" : driverUid
        ]
    }
}

