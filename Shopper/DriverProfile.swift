//
//  DriverProfile.swift
//  Shopper
//
//  Created by VividMacmini7 on 05/04/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

private enum SelectedOption:Int {
    case OptionWorkArea = 0
    case OptionOrderToBeDelivred
}


class DriverProfile: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: @IBOutlet:
    @IBOutlet var avatar:UILabel!
    @IBOutlet var avatarBG:UIView!
    @IBOutlet var driverName:UILabel!
    @IBOutlet var driverAddress:UILabel!
    @IBOutlet var driverEmail:UILabel!
    @IBOutlet var workArea:UIButton!
    @IBOutlet var latestOrders:UIButton!
    @IBOutlet var takeOrder:UIButton!
    
    @IBOutlet var statusView:UIView!
    @IBOutlet var areaListTable:UITableView!
    
    @IBOutlet var centerConstraint:NSLayoutConstraint!
    @IBOutlet var widthConstraint:NSLayoutConstraint!
    
    private var optionType = SelectedOption.OptionWorkArea
    
    var driverDetails:DriverModel!
    var customerList = [CustomerModel]()
    var orderToDeliver = [OrderDetailModel]()
    
    var areaList = [String]()
    var areaWithPendingOrder = [String]()
    
    let customerRef = FIRDatabase.database().referenceWithPath("customer_list")
    var orderDetailRef = FIRDatabase.database().referenceWithPath("order_Details")
    var driverRef = FIRDatabase.database().referenceWithPath("driver_list")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let colorTop = UIColor.darkGrayColor().CGColor
        let colorBottom = UIColor.lightGrayColor().CGColor
        
        let gl = CAGradientLayer()
        gl.colors = [colorTop, colorBottom]
        gl.locations = [0.0, 1.0]
        
        avatarBG.backgroundColor = UIColor.clearColor()
        gl.frame = CGRectMake(0, 0, 50, 50)
        avatarBG.layer.insertSublayer(gl, atIndex: 0)
        
        areaListTable.estimatedRowHeight = 55
        areaListTable.tableFooterView = UIView()
        statusView.hidden = true
        
        let user = HelperClass.userDetails()
        if user!.uType == .UserTypeDriver {
//            takeOrder.setTitle("Take Order", forState: .Normal)
//            var button = UIButton()
//            button.frame = CGRectMake(0, 0, 32, 32)
//            button.setImage(UIImage(named: "package"), forState: .Normal)
//            button.addTarget(self, action: #selector(packOrders(_:)), forControlEvents: .TouchUpInside)
//            button.tintColor = UIColor.appTheme()
            
            //            let rightBarButton = UIBarButtonItem()
            //            rightBarButton.customView = button

//            centerConstraint.constant = 50
//            widthConstraint.constant = 0
            
            
            let button = UIButton()
            button.frame = CGRectMake(0, 0, 32, 32)
            button.setImage(UIImage(named: "logout"), forState: .Normal)
            button.addTarget(self, action: #selector(logout(_:)), forControlEvents: .TouchUpInside)
            button.tintColor = UIColor.appTheme()
            
            let rightBarButton1 = UIBarButtonItem()
            rightBarButton1.customView = button
            
            let space = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
            space.width = 30
            self.navigationItem.rightBarButtonItems = [rightBarButton1,space]
        }else{
            centerConstraint.constant = 50
            widthConstraint.constant = 0
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        if driverDetails == nil {
            self.driverRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
                if !(snapshot.value is NSNull) {
                    for child in snapshot.children {
                        let driver = DriverModel(snapshot: child as! FIRDataSnapshot)
                        if driver.key == HelperClass.userDetails()?.uId {
                            self.driverDetails = driver
                            dispatch_async(dispatch_get_main_queue(), {
                                self.initialSetup()
                            })
                        }
                    }
                }
            })
        }else {
            initialSetup()
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initialSetup() {
        let name = driverDetails.driverName
        let seprator = name .componentsSeparatedByString(" ")
        let characterOne = seprator[0].characters
        if seprator.count > 1 {
            let characterTwo = seprator[1].characters.count != 0 ? seprator[1].characters : characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(characterTwo.first!)"
            avatar.text = avatorText.uppercaseString
        }else{
            let secondChar = characterOne.dropFirst()
            let avatorText = "\(characterOne.first!)\(secondChar.first!)"
            avatar.text = avatorText.uppercaseString
        }
        
        driverAddress.text = driverDetails.driverAddress
        driverEmail.text = driverDetails.driverEmail
        driverName.text = driverDetails.driverName
        
        getReleventCustomerList()
    }
    
    func getReleventCustomerList() {
        customerList = [CustomerModel]()
        areaList = [String]()
        customerRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                let backgroundImage = UIImage(named: "TableBG1")
                let imageView = UIImageView(image: backgroundImage)
                
                for child in snapshot.children {
                    let customer = CustomerModel(snapshot: child as! FIRDataSnapshot)
                    let workAreas = self.driverDetails.workArea
                    if workAreas != nil {
                        self.areaList = workAreas!
                        if workAreas!.contains(customer.shopArea) {
                            self.customerList.append(customer)
                        }
                    }else{
                        self.areaListTable.backgroundView = imageView
                    }
                }
                if self.customerList.count == 0 {
                    self.areaListTable.backgroundView = imageView
                }
                dispatch_async(dispatch_get_main_queue(), {
                    self.areaList = Array(Set(self.areaList))
                    self.areaListTable.reloadData()
                })
                
            }
        })
    }
    //MARK: UITableViewDataSource:
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return  optionType == .OptionOrderToBeDelivred ? areaWithPendingOrder.count : areaList.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if optionType == .OptionWorkArea {
            let section = areaList[section]
            var i = 0
            for content in customerList {
                if section == content.shopArea {
                    i += 1
                }
            }
            return i
        }else if optionType == .OptionOrderToBeDelivred {
            var shopAreas = [String]()
            for order in orderToDeliver {
                shopAreas.append(order.shopArea)
            }
            let section = areaWithPendingOrder[section]
            var i = 0
            for area in shopAreas {
                if section == area {
                    i += 1
                }
            }
            return i
        }else{
            return 1
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if optionType == .OptionWorkArea {
            let cell:CustomerListCell = tableView.dequeueReusableCellWithIdentifier("DriverAreaListCell", forIndexPath: indexPath) as! CustomerListCell
            
            var item = [CustomerModel]()
            let section = areaList[indexPath.section]
            for content in customerList {
                if section == content.shopArea {
                    item.append(content)
                }
            }
            cell.shopName.text = item[indexPath.row].shopName
            cell.customerName.text = item[indexPath.row].customerName
            cell.customerPhone.text = item[indexPath.row].customerPhone
            let name = item[indexPath.row].shopName
            let seprator = name .componentsSeparatedByString(" ")
            let characterOne = seprator[0].characters
            if seprator.count > 1 {
                let characterTwo = seprator[1].characters.count != 0 ? seprator[1].characters : characterOne.dropFirst()
                let avatorText = "\(characterOne.first!)\(characterTwo.first!)"
                cell.avatar.text = avatorText.uppercaseString
            }else{
                let secondChar = characterOne.dropFirst()
                let avatorText = "\(characterOne.first!)\(secondChar.first!)"
                cell.avatar.text = avatorText.uppercaseString
            }
            return cell
        } else if optionType == .OptionOrderToBeDelivred {
            let cell:LatestOrderCell = tableView.dequeueReusableCellWithIdentifier("DriverOrderCell", forIndexPath: indexPath) as! LatestOrderCell
            
            var orderDetails = [OrderDetailModel]()
            var shopAreas = [String]()
            for order in orderToDeliver {
                shopAreas.append(order.shopArea)
            }
            let section = areaWithPendingOrder[indexPath.section]
            for area in orderToDeliver {
                if section == area.shopArea {
                    orderDetails.append(area)
                }
            }
            
            cell.orderAmount.text = "\(orderDetails[indexPath.row].orderPrice) ₹"
            cell.shopName.text = orderDetails[indexPath.row].shopName
            cell.orderDate.text = orderDetails[indexPath.row].orderDate
            
            let status = orderDetails[indexPath.row].orderStatus
            if status == .OrderTaken {
                cell.connectorOne.backgroundColor = UIColor.lightGrayColor()
                cell.connectorTwo.backgroundColor = UIColor.lightGrayColor()
                
                cell.orderTaken.backgroundColor = UIColor.appThemeYellow()
                cell.packed.backgroundColor = UIColor.lightGrayColor()
                cell.delivered.backgroundColor = UIColor.lightGrayColor()
                
            }else if status == .OrderPacked {
                cell.connectorOne.backgroundColor = UIColor.appThemeGreen()
                cell.connectorTwo.backgroundColor = UIColor.lightGrayColor()
                
                cell.orderTaken.backgroundColor = UIColor.appThemeGreen()
                cell.packed.backgroundColor = UIColor.appThemeGreen()
                cell.delivered.backgroundColor = UIColor.lightGrayColor()
                
            }else if status == .OrderDelivered {
                cell.connectorOne.backgroundColor = UIColor.appTheme()
                cell.connectorTwo.backgroundColor = UIColor.appTheme()
                
                cell.orderTaken.backgroundColor = UIColor.appTheme()
                cell.packed.backgroundColor = UIColor.appTheme()
                cell.delivered.backgroundColor = UIColor.appTheme()
                
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCellWithIdentifier("AreaCell", forIndexPath: indexPath)
            cell.textLabel?.text = areaList[indexPath.section]
            return cell
        }
        
        
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,40))
        headerView.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
        let areaName = UILabel(frame:  CGRectMake(15,0,self.view.frame.size.width/2,40))
        areaName.text =  optionType == .OptionOrderToBeDelivred ? areaWithPendingOrder[section] : areaList[section]
        areaName.textColor = UIColor.blackColor()
        areaName.font = UIFont.AppFontBold()
        headerView.addSubview(areaName)
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if optionType == .OptionWorkArea {
            var item = [CustomerModel]()
            let section = areaList[indexPath.section]
            for content in customerList {
                if section == content.shopArea {
                    item.append(content)
                }
            }
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("CustomerProfile") as! CustomerProfile
            vc.customerDetails = item[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if optionType == .OptionOrderToBeDelivred {
            var orderDetails = [OrderDetailModel]()
            var shopAreas = [String]()
            for order in orderToDeliver {
                shopAreas.append(order.shopArea)
            }
            let section = areaWithPendingOrder[indexPath.section]
            for area in orderToDeliver {
                if section == area.shopArea {
                    orderDetails.append(area)
                }
            }
            
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("OrderProfile") as! OrderProfile
            vc.orderIdStr = orderDetails[indexPath.row].orderId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: @IBAction:
    @IBAction func logout(sender:UIBarButtonItem) {
        let alert = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: .ActionSheet)
        alert.addAction(UIAlertAction(title: "Logout", style: .Destructive, handler: { (action) in
            NSUserDefaults.standardUserDefaults().removeObjectForKey((HelperClass.userDetails()?.uId)!)
            if FIRAuth.auth()?.currentUser != nil {
                do {
                    try FIRAuth.auth()?.signOut()
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Loign") as! Loign
                    let navVC = UINavigationController(rootViewController: vc)
                    appDelegate.window?.rootViewController = navVC
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
        
    }
    
//    @IBAction func packOrders(sender:UIBarButtonItem) {
//        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SalesmanPackOrder") as! SalesmanPackOrder
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    
    @IBAction func workArea(sender:UIButton) {
        optionType = .OptionWorkArea
        statusView.hidden = true
        
        workArea.backgroundColor = UIColor.appTheme()
        latestOrders.backgroundColor = UIColor.darkGrayColor()
        takeOrder.backgroundColor = UIColor.darkGrayColor()
        
        self.areaListTable.backgroundView = UIView()
        areaListTable.reloadData()
    }
    
    
    @IBAction func orderToBeDelivered(sender:UIButton) {
        areaWithPendingOrder = [String]()
        self.orderToDeliver = [OrderDetailModel]()
        
        
        guard let workArea = driverDetails.workArea else { return }
        orderDetailRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let detail = OrderDetailModel(snapshot: child as! FIRDataSnapshot)
                    if workArea.contains(detail.shopArea) && detail.orderStatus != OrderStatus.OrderDelivered {
                        self.orderToDeliver.append(detail)
                        self.areaWithPendingOrder.append(detail.shopArea)
                    }
                }
                
                self.areaWithPendingOrder = Array(Set(self.areaWithPendingOrder))
                if self.orderToDeliver.count == 0 {
                    let backgroundImage = UIImage(named: "TableBG1")
                    let imageView = UIImageView(image: backgroundImage)
                    self.areaListTable.backgroundView = imageView
                }
                self.optionType = .OptionOrderToBeDelivred
                self.statusView.hidden = false
                
                self.latestOrders.backgroundColor = UIColor.appTheme()
                self.workArea.backgroundColor = UIColor.darkGrayColor()
                self.takeOrder.backgroundColor = UIColor.darkGrayColor()
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.areaListTable.reloadData()
                })
                
            }
        })
        
    }
    
    func isLessThanDate(dateOne: String, dateTwo: String) -> Bool {
        //Declare Variables
        var isLess = false
        
        let formate = NSDateFormatter()
        formate.dateFormat = "dd-MM-yyyy hh:mm a"
        let date1 = formate.dateFromString(dateOne)
        let date2 = formate.dateFromString(dateTwo)
        //Compare Values
        if date1!.compare(date2!) == NSComparisonResult.OrderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    @IBAction func thirdOption(sender:UIButton) {
        var orderList = [OrderDetailModel]()
        guard let workArea = driverDetails.workArea else { return }
        orderDetailRef.observeSingleEventOfType(.Value, withBlock: { snapshot in
            if snapshot.value is NSNull {
                
            } else {
                for child in snapshot.children {
                    let detail = OrderDetailModel(snapshot: child as! FIRDataSnapshot)
                    if workArea.contains(detail.shopArea) && detail.orderStatus != OrderStatus.OrderDelivered {
                        orderList.append(detail)
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("DriverMapView") as! DriverMapView
                    vc.deliveryList = orderList
                    self.navigationController?.pushViewController(vc, animated: true)
                })
                
            }
        })
        
        
 
    }
    
    @IBAction func editProfile(sender:UIBarButtonItem) {
        let vc:AddSalesman = self.storyboard?.instantiateViewControllerWithIdentifier("AddSalesman") as! AddSalesman
        vc.driverDetails = driverDetails
        vc.isAddDriver = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func callSalesman(sender:UIBarButtonItem) {
        let url:NSURL = NSURL(string: "tel:\(driverDetails.driverPhone)")!
        UIApplication.sharedApplication().openURL(url)
    }
}

