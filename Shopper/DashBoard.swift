//
//  DashBoard.swift
//  Shopper
//
//  Created by VividMacmini7 on 27/03/17.
//  Copyright © 2017 vivid. All rights reserved.
//

import UIKit
import FirebaseAuth

class DashBoard: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    var dashboardList = ["Stock \nList","Area", "Customer","Salesman","Driver","Manager"]
    var userId:String!
    
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userId = (HelperClass.userDetails()?.uId)!
        collectionView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:UICollectionViewDataSource:
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dashboardList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:DashBoardCell = collectionView.dequeueReusableCellWithReuseIdentifier("DashBoardCell", forIndexPath: indexPath) as! DashBoardCell
        cell.dashboardItem.text = dashboardList[indexPath.item]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("StockList") as! StockList
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("AreaList") as! AreaList
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.item == 2 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Customers") as! Customers
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.item == 3 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("SalesMan") as! SalesMan
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.item == 4 {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Driver") as! Driver
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:UICollectionViewDelegateFlowLayout:
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((collectionView.frame.size.width-30)/2, (collectionView.frame.size.width-30)/2)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    //MARK: IBaction:
    @IBAction func logout(sender:UIBarButtonItem) {
        let alert = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: .ActionSheet)
        alert.addAction(UIAlertAction(title: "Logout", style: .Default, handler: { (action) in
            NSUserDefaults.standardUserDefaults().removeObjectForKey((HelperClass.userDetails()?.uId)!)
            if FIRAuth.auth()?.currentUser != nil {
                do {
                    try FIRAuth.auth()?.signOut()
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Loign") as! Loign
                    let navVC = UINavigationController(rootViewController: vc)
                    appDelegate.window?.rootViewController = navVC
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
}


class DashBoardCell: UICollectionViewCell {
    @IBOutlet var dashboardItem: UILabel!
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 10.0
        dashboardItem.textColor = UIColor.whiteColor()
        dashboardItem.numberOfLines = 0
    }
}



